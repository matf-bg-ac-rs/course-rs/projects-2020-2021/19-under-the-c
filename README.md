# Project 19-Under-the-C
🍓 Demo snimak: https://drive.google.com/file/d/1qKwlbRyV70uofJHHg3TX2vzwWxK67p-0/view?usp=drive_web 

🍎 Walkthrough drugog nivoa: https://drive.google.com/file/d/11X2XCKbZpx2YryrMEro0-V2LMMRL3eYx/view

🍊 Walkthrough cetvrtog nivoa: https://drive.google.com/file/d/1-8o5pJGbtWmgJgSn21CXmClN275ntynY/view?usp=sharing

🍒 Tutorijal: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/19-under-the-c/-/wikis/Tutorijal

🍉 Pokretanje: https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/19-under-the-c/-/wikis/Pokretanje

Under the C je 2D igrica koja prati 4 devojke u njihovoj avanturi da nadju blago skriveno u zamku ispod mora. Igrica ima ukupno cetiri nivoa i u svakom nivou igrac upravlja razlicitim likom. Osim razlike u izgledu likova njih razlikuju i drugacije logike nivoa kojih je cak tri kao i sredina izgleda scene. Verujemo da koriscenjem razlicitih logika nivoa budimo mastu korisnika koji ce prizeljkivati da vidi sta je to novo sto ce sledeci nivo doneti, ali isto tako to ce igracu biti izazov da uvezba da se brze prilagodi drugacijim izazovima i stekne vise razlicitih vestina igrajuci jednu igru. Na kraju prva tri nivoa zadatak igraca jeste da na razlicite nacine oslobodi sledeceg lika kojim ce upravljati u narednom nivou. Prelaskom cetvrtog nivoa dolazi se do kraja igrice gde su svi prethodni igraci zajedno kod blaga. 

## Developers

- [Angelina Jordanov, 422/2019](https://gitlab.com/Sogeking099)
- [Katarina Popovic, 361/2020](https://gitlab.com/katarina.popovic)
- [Irina Marko, 243/2017](https://gitlab.com/irinamarko)
- [Milana Novakovic, 467/2018](https://gitlab.com/milana98)
