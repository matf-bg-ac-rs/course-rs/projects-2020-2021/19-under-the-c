#include "Headers/TextOfNotice.h"

TextOfNotice::TextOfNotice()
{
    setPos(500, 300);
    setScale(1.5);
    setDefaultTextColor(Qt::black);
    onScene=false;

}

void TextOfNotice::setText(QString text)
{
    setPlainText(text);
}

void TextOfNotice::setDuration(int sek)
{
    int tmp=(sek*1000)/50;
    duration=tmp;
}

int TextOfNotice::getDuration()
{
   int tmp=duration;
   duration--;
   return tmp;
}

bool TextOfNotice::IsOnScene()
{
    return onScene;
}

void TextOfNotice::ItemOnScene(bool value)
{
    onScene=value;
}
