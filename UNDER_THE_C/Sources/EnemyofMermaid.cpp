
#include <Headers/EnemyofMermaid.h>
#include <QTimer>
#include <QDebug>
#include <QGraphicsScene>
#include <stdlib.h>
#include <QDebug>
#include "Headers/Health.h"
#include "Headers/Score.h"
#include "Headers/Game.h"
#include<QString>
#include <Headers/Bubble.h>
#include <iostream>


double EnemyofMermaid::speedUp=1;


EnemyofMermaid::EnemyofMermaid(QGraphicsPixmapItem *parent): ItemOfScene(parent)
{
      int random=rand()%700;
      int randomPicture= rand()%8;
      QString name = QString::number(randomPicture);
      name.append(".png");
      QString fullName= ":/images/enemies/squids/";
      fullName.append(name);
      setPixmap(QPixmap(fullName));

      setPos(1280,random);

      health_change=0;
      score_change = 0;

      setIsAlive(true);
      setType(ENEMYOFMERMAID);

      healthBar= new HealthBar(x(), y()-5,80,10);
      lives=3;
}

void EnemyofMermaid::move(){

    if(getIsAlive()==false)
        return;

    setPos(x()-5, y());
    healthBar->bar->setPos(healthBar->bar->x()-5, healthBar->bar->y());
    healthBar->barFrame->setPos(healthBar->barFrame->x()-5, healthBar->barFrame->y());

    if(pos().x()+width()<0){
        setIsAlive(false);
        scene()->removeItem(this->healthBar->bar);
        scene()->removeItem(this->healthBar->barFrame);
        delete(this->healthBar->barFrame);
        delete(this->healthBar->bar);
    }
    collision();
}

void EnemyofMermaid::collision()
{
    auto colliding_items= collidingItems();
        foreach(auto item, colliding_items){

            if(isAlive == false)
                return;

            if(typeid (*item)== typeid(PlayerMermaid) ){

            if(qgraphicsitem_cast<PlayerMermaid *>(item)->getShiled()==false){
                setHealthChange(1);
            }
            else{
                qgraphicsitem_cast<PlayerMermaid *>(item)->setShiled(false);
                setHealthChange(0);
            }

            setIsAlive(false);
            qDebug()<<"pre baga";
          scene()->removeItem(this->healthBar->bar);
            scene()->removeItem(this->healthBar->barFrame);
            qDebug()<<"posle baga";
            delete(this->healthBar->barFrame);
            delete(this->healthBar->bar);
            return;
        }
        else if( typeid (*item)== typeid(Bubble) && getIsAlive() == true){

            setScoreChange(1);
            qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);


            if(this->getNumberOfLives()==3){


                scene()->removeItem(this->healthBar->bar);
                delete(this->healthBar->bar);
                this->healthBar->bar =new QGraphicsRectItem (x(), y()-5, 55,10);
                this->healthBar->bar->setBrush(Qt::darkCyan);
                qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                scene()->addItem(this->healthBar->bar);
                this->decreaseNumberofLives();
            }
           else if(this->getNumberOfLives()==2){

                scene()->removeItem(this->healthBar->bar);
                delete(this->healthBar->bar);
                this->healthBar->bar =new QGraphicsRectItem (x(), y()-5, 30,10);
                this->healthBar->bar->setBrush(Qt::red);
                qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                scene()->addItem(this->healthBar->bar);
                this->decreaseNumberofLives();
            }
            else{
                setScoreChange(1);
                setIsAlive(false);
                 scene()->removeItem(this->healthBar->bar);
                scene()->removeItem(this->healthBar->barFrame);

                qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
                this->decreaseNumberofLives();
                delete(this->healthBar->barFrame);
                delete(this->healthBar->bar);

                }
            }
         }
}


int EnemyofMermaid::width()
{
    return boundingRect().width()*0.05;
}

int EnemyofMermaid::getNumberOfLives()
{
    return lives;
}

void EnemyofMermaid::decreaseNumberofLives()
{
    lives--;
}
