#include "Headers/LevelPlatform.h"

LevelPlatform::LevelPlatform(QObject *parent):
    LevelScene(parent)
{
    pause=0;
    stopEventKey=0;

}

void LevelPlatform::DeleteLevelPlatform()
{

    int key_value=-1;

    for(auto it=items.begin(); it!=items.end(); it++){

        if(key_value != -1){
            auto value=items.take(key_value);
            removeItem(value);
            delete value;
        }
        key_value = it.key();
    }

    auto value=items.take(key_value);
    removeItem(value);
    delete value;

    for(int i=0; i < objects.length(); i++){
        delete objects[i];
    }
    objects.clear();

    delete player;
    delete h;
    delete pScore;
    delete s;
    delete sword;
    if(onRules)
        removeItem(rules);
    delete buttonRules;
    delete musicButton;
    deleteSounds();

};

void LevelPlatform::initialize()
{
    soundTimer=0;
    battle_activated = false;
    //0
    pushBackSound("qrc:/sounds/cristal_collected.mp3");
    //1
    pushBackSound("qrc:/sounds/deat_ray.mp3");
    //2
    pushBackSound("qrc:/sounds/door_open.mp3");
    //3
    pushBackSound("qrc:/sounds/hit.mp3");
    //4
    pushBackSound("qrc:/sounds/koko_background.mp3");
    //5
    pushBackSound("qrc:/sounds/koko_battle.mp3");
    //6
    pushBackSound("qrc:/sounds/life_collected.mp3");
    //7
    pushBackSound("qrc:/sounds/spider_laugh_activated.mp3");
    //8
    pushBackSound("qrc:/sounds/spider_laugh_attack.mp3");
    //9
    pushBackSound("qrc:/sounds/sword.mp3");
    //10
    pushBackSound("qrc:/sounds/walking2.mp3");
    checkMute();
    makeSound(4);

    musicButton= new Button();
    musicButton->initialize(":/images/buttons/speakerWhite.png", 0.03, 20, 50);
    addItem(musicButton);
    connect(musicButton, SIGNAL(clicked()),this, SLOT(onSetMute()) );
    if(mute==true)
        musicButton->initialize(":/images/buttons/muteWhite.png", 0.03, 5, 55);

    buttonRules=new Button();
    buttonRules->initialize(":/images/buttons/PinkyButton.png", 1, 500,500);
    connect(buttonRules, SIGNAL(clicked()),this, SLOT(onClicked()) );

    setBackgroundBrush(QPixmap(":/images/backgrounds/NebulaStars.png"));

    QString filename;
    filename = ":/sceneDesign.txt";

    QFile file(filename);

    if(!file.exists()){
        std::cout << "Fajl ne postoji!" << std::endl;
        return ;
    }
    if(!file.open(QIODevice::ReadOnly)){
        std::cout << "Nije uspelo otvaranje fajla!" << std::endl;
        return;
    }

    QTextStream in(&file);

    // U prvoj liniji su dimenzije matrice
    QStringList line = in.readLine().split(" ");
    int sizeX = line[0].toInt();
    int sizeY = line[1].toInt();

    // 80x70
    setSceneRect(0, 0, 80*(sizeX), sizeY*70);

    for(int y=0; y<sizeY; y++){
        QString linija = in.readLine();

        for(int x = 0; x<sizeX; x++){

            auto polje = linija.toStdString().c_str()[x];

            if(polje == '-')
                continue;

            //Dodavanje i pozicioniranje igraca na scenu
            if(polje == 'p'){
                player = new PlayerPlatform();
                addItem(player);
                setFocus(Qt::TabFocusReason);
                player->setFocus();
                player->setPos(x*80, y*70);
            }
            //Dodavanje i pozicioniranje duhova na scenu
            else if(polje == 'e'){
                EnemyFly* e = new EnemyFly();
                e->setPos(x*80,y*70);
                addItem(e);

                items.insert(e->getID(), e);
                e->incID();
            }
            //Dodavanje i pozicioniranje pauka koji puca na scenu
            else if(polje == 'E'){
                auto e = new EnemySpider();
                e->setPos(x*80,y*70);
                addItem(e);

                items.insert(e->getID(), e);
                e->incID();
            }
            //Dodavanje i pozicioniranje glavnog neprijatelja na scenu
            else if(polje == 'B'){
                SpiderBoss *boss = new SpiderBoss();
                boss->setPos(x*80,y*70);
                addItem(boss);

                items.insert(boss->getID(), boss);
                boss->incID();
            }
            //Dodavanje i pozicioniranje trnja na scenu
            else if(polje == 'T'){
                EnemyThorn *t = new EnemyThorn();
                t->setRect(x*80-20,y*70+10,200,70);
                addItem(t);
                objects.push_back(t);
            }
            //Dodavanje i pozicioniranje ukrasnih objekata na scenu
            else if(polje == 'r' || polje == 't' || polje == 'l' || polje == 'k' || polje==';' || polje == 'o'
                    || polje == 'g' || polje == 'h' || polje == 'j' || polje == 'd' || polje == 'f' || polje == 'M'){
                SceneDecoration* s = new SceneDecoration(polje, x, y);
                addItem(s);
                objects.push_back(s);
            }
            //Dodavanje i pozicioniranje kristala na scenu
            else if(polje == 'C' || polje == 'K' || polje == 'L'){
                auto d = new Cristal(polje);
                d->setPos(x*80, y*70);
                addItem(d);

                items.insert(d->getID(), d);
                d->incID();
            }
            //Dodavanje i pozicioniranje vrata na scenu
            else if(polje == 'W' || polje == 'G'){
                Door* d = new Door(polje);
                d->setPos(x*80, y*70);
                addItem(d);

                items.insert(d->getID(), d);
                d->incID();
            }
            //Dodavanje i pozicioniranje platformi na scenu
            else{
                Platform* p = new Platform(polje);
                p->setRect(0, 0, 80, 70);
                p->setPos(x*80, y*70);

                addItem(p);
                objects.push_back(p);
                //addPlatform(polje,x,y,80,70);
            }
        }
    }

    h  = new Health();
    h->setHealth(5);
    h->setDefaultTextColor(Qt::red);
    addItem(h);

    pScore = new Score(1);
    addItem(pScore);

    s = new Score(4);
    addItem(s);

    sword = new Sword();
    onRules=false;
}

void LevelPlatform::onSetMute()
{
    setMute();
    if(mute)
        musicButton->initialize(":/images/buttons/muteWhite.png", 0.03, 5, 55);
    else
        musicButton->initialize(":/images/buttons/speakerWhite.png", 0.03, 5, 55);
    checkMute();
}

void LevelPlatform::update()
{


    if(soundTimer==4000 && battle_activated == false){
        //qDebug()<<"USAO";
        makeSound(4);
        soundTimer=0;
    }
    if(soundTimer2==2800 && battle_activated == true){
        makeSound(5);
        soundTimer2=0;
    }
    if(!battle_activated)
        soundTimer++;

    if(battle_activated)
        soundTimer2++;

    player->move();

    if(player->getMusicEmit() == 1 && player->getMusicTimer() == 0){
        makeSound(10);
        player->setMusicEmit(0);
    }
    if(player->getMusicTimer() > 0){
        player->increaseMusicTimer();

        if(player->getMusicTimer() == 7)
            player->setMusicTimer(0);
    }

    player->image_change();
    player->collisions();

    if(player->getHealthChange() > 0){
        h->decrease();
        player->setHealthChange(0);
    }

    if(sword->getExistance() == -1){
         removeItem(sword);
         sword->setExistance(0);
    }
    if(sword->getExistance() == 1){
        makeSound(9);
        addItem(sword);
    }

    sword->move(player->x(), player->y(), player->left_right_jump[0], player->left_right_jump[1], player->last);

    //pomeranje i kolizija objekata na sceni
    if(!items.empty()){

        for(auto it=items.begin(); it!=items.end(); it++){

            if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMYGHOST){

                auto enemy = (qgraphicsitem_cast<EnemyFly *>(it.value()));
                if(enemy->getNearPlayer() == false){
                    enemy->findPlayer();
                }
                else if(enemy->getTimeout() == 0){
                    enemy->move(player->x(), player->y());
                    enemy->collisions();
                }
                else
                    enemy->decraseTimeout();

                enemy->image_change();

                if(enemy->getHealthChange() > 0){
                    h->decrease();
                    enemy->setHealthChange(0);
                }

                if(enemy->getMusicEmit()==1){
                    makeSound(3);
                    qDebug()<<"udarac";
                }
                enemy->setMusicEmit(0);
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMYSPIDER){
                auto enemy = (qgraphicsitem_cast<EnemySpider *>(it.value()));

                enemy->findPlayer();
                enemy->image_change();

                if(enemy->getAttackMode()){
                    EnemyDeathRay* ray = new EnemyDeathRay(player->x(), player->y(), enemy->x(), enemy->y());
                    items.insert(ray->getID(), ray);
                    ray->incID();
                    addItem(ray);

                    enemy->setAttackMode(false);
                }

                enemy->collision();

                if(enemy->getTimeout() > 0)
                    enemy->decreaseTimeout();

                if(enemy->getHealthChange() > 0){
                    h->decrease();
                    enemy->setHealthChange(0);
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::DEATHRAY){
                auto ray = (qgraphicsitem_cast<EnemyDeathRay *>(it.value()));

                ray->incActivated();
                ray->update(QRect(ray->getEX()-41*80*2,ray->getEY()-9*70*2, 80*80*2,30*70*2));

                if(ray->getActivated() == 20){
                    makeSound(1);
                }

                if(ray->getActivated() >= 40){
                    ray->setIsAlive(false);
                }
                ray->collision();
                if(ray->getHealthChange() > 0){
                    h->decrease();
                    ray->setHealthChange(0);
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::SPIDERBOSS){
                auto boss = (qgraphicsitem_cast<SpiderBoss *>(it.value()));

                if(boss->getBattle() == false){
                    boss->findPlayer();
                    if(boss->getBattle()){
                        battle_activated = true;
                        soundTimer2=0;
                        stopSound(4);
                        makeSound(5);
                    }
                }


                if(boss->getBattle()){

                    boss->move(player->x());
                    boss->image_change();
                    boss->collisions();

                    if(boss->getDefense() == 200){

                        int attack_type = boss->attack();
                        if(attack_type == 2){
                            bossAttack2();
                            boss->startDefense();
                        }
                    }
                    else{
                        boss->increaseDefense();
                    }

                    if(boss->getTimeout() != 0)
                        boss->decraseTimeout();

                    if(boss->getHealthChange() > 0){
                        h->decrease();
                        boss->setHealthChange(0);

                    }
                }
                if(boss->getMusicEmit()==1){
                    makeSound(7);
                }else if(boss->getMusicEmit()==2){
                    makeSound(3);
                }else if(boss->getMusicEmit()==3){
                    makeSound(8);
                }

                boss->setMusicEmit(0);

            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::CRISTAL){
                auto cristal = (qgraphicsitem_cast<Cristal *>(it.value()));

                cristal->collision();
                if(cristal->getMusicEmit()==1){
                    makeSound(0);
                }
                if(cristal->getScoreChange() > 0){
                    pScore->increase();
                    cristal->setScoreChange(0);
                }
                if(cristal->getGoldenCollected())
                    s->increase();

                cristal->setMusicEmit(0);
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::DOOR){
                auto door = (qgraphicsitem_cast<Door *>(it.value()));

                door->collision();

                if(door->getMusicEmit()==1){
                    makeSound(2);
                }
                door->setMusicEmit(0);
            }
        }
    }

    if(h->getHealth() <= 0){
        DeleteLevelPlatform();
        emit lifeLost();
    }

    //*** BRISANJE "mrtvih" elemenata ********//
    int key_check = -1;

    if(!items.empty()){
        for(auto it=items.begin(); it!=items.end(); it++){

            if(key_check != -1){

                auto values=items.take(key_check);
                removeItem(values);
                delete values;

                key_check = -1;
            }

            if(it.value()->getIsAlive() == false){

                if(it.value()->getType() == ItemOfScene::typeOfItem::CRISTAL){
                    auto cristal = (qgraphicsitem_cast<Cristal *>(it.value()));

                    if(cristal->getType() == 'L')
                        emit lifeCollected();

                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::SPIDERBOSS){

                    DeleteLevelPlatform();
                    emit levelCompleted();
                    return;
                }

                key_check=it.key();

            }
        }
    }

    if(key_check != -1){
        auto values=items.take(key_check);
        removeItem(values);
        delete values;

        key_check = -1;
    }

    emit center(player,h, pScore, s, rules, buttonRules, musicButton);

}

void LevelPlatform::onClicked()
{
    removeItem(rules);
    removeItem(buttonRules);
    pause=!pause;
    emit gamePaused();
    playSounds();
    stopEventKey=false;
    onRules=false;
}

void LevelPlatform::bossAttack2()
{
    EnemyFly* e = new EnemyFly();
    e->setPos(4*80,28*70);
    e->setNearPlayer(true);
    addItem(e);
    items.insert(e->getID(), e);
    e->incID();

    e = new EnemyFly();
    e->setPos(40*80,28*70);
    e->setNearPlayer(true);
    addItem(e);
    items.insert(e->getID(), e);
    e->incID();
}

void LevelPlatform::keyPressEvent(QKeyEvent *event)
{

    if(event->key()==Qt::Key_P && !onRules){
       emit gamePaused();
        if(!pause ){
            stopEventKey=true;
            pauseSonuds();
        }else{
            stopEventKey=false;
            playSounds();

        }
        pause=!pause;

    }else if(event->key()==Qt::Key_M){

       setMute();
       if(mute)
           musicButton->initialize(":/images/buttons/muteWhite.png", 0.03, 5, 55);
       else
           musicButton->initialize(":/images/buttons/speakerWhite.png", 0.03, 5, 55);
       checkMute();
    }
    if(event->key()==Qt::Key_B){
        DeleteLevelPlatform();
        emit gameOver();
    }
    if(event->key()==Qt::Key_1){
        DeleteLevelPlatform();
        emit level(0);
        return;
    }
    if(event->key()==Qt::Key_2){
        DeleteLevelPlatform();
        emit level(1);
    }
    if(event->key()==Qt::Key_3){
        DeleteLevelPlatform();
        emit level(2);
    }
    if(event->key()==Qt::Key_4){
        DeleteLevelPlatform();
        emit level(3);
    }

    if(event->isAutoRepeat() == false && event->key()==Qt::Key_H && !onRules){

        if(!pause){

            emit gamePaused();
            stopEventKey=true;
            pauseSonuds();
            pause=!pause;
        }
        rules->setNumberOfLevel(2);
        buttonRules->setZValue(10);
        addItem(rules);
        addItem(buttonRules);
        onRules=true;


    }
    if(stopEventKey)
        return;
    if(event->isAutoRepeat() == false && event->key() == Qt::Key_Left){
        player->left_right_jump[0] = 1;
        player->left_right_jump[1] = 0;
    }
    else if(event->isAutoRepeat() == false && event->key() == Qt::Key_Right){
        player->left_right_jump[1] = 1;
        player->left_right_jump[0] = 0;
    }
    else if(event->isAutoRepeat() == false && event->key() == Qt::Key_Up){
            if(player->standing){
                player->left_right_jump[2] = 1;
                player->jump_height = 16;
            }
    }
    else if(event->isAutoRepeat() == false && event->key() == Qt::Key_Space && sword->getExistance() == 0){
        sword->setExistance(1);
    }

}
void LevelPlatform::keyReleaseEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat() == false && event->key() == Qt::Key_Left){
        player->left_right_jump[0] = 0;
        player->last = 1;
    }
    if(event->isAutoRepeat() == false && event->key() == Qt::Key_Right){
        player->left_right_jump[1] = 0;
        player->last = 2;
    }
   if(event->isAutoRepeat() == false && event->key() == Qt::Key_Up){
        if(player->left_right_jump[2] < 10){
             player->jump_height = 10;
        }
   }
}
