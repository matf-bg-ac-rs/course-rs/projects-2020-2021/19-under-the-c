#include "Headers/Button.h"
#define UNUSED(expr) do { (void)(expr); } while (0)

Button::Button(QGraphicsItem *parent):QGraphicsPixmapItem(parent)
{


}

Button::Button(int number)
{
   chosen_level=number;
}




void Button::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    UNUSED(event);

    emit clicked();
    emit clicked_level(chosen_level);


}



void Button::initialize(QString name, double scale, double width, double height)
{

    setPixmap(QPixmap(name));
    setScale(scale);
    setPos(width,height);
}


