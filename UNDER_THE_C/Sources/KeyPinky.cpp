#include <QGraphicsScene>
#include "Headers/KeyPinky.h"
#include "QPainter"
#include <stdlib.h>
#include <QTimer>
#include <QDebug>
#include <Headers/PlayerPinky.h>
int KeyPinky::brojac=0;
KeyPinky::KeyPinky(QGraphicsItem *parent)
{
        setType(KEYP);
        setIsAlive(true);
        setPixmap(QPixmap(":/images/collectables/golden_key.png"));
        setScale(0.15);


        ID=brojac;
        brojac++;

}

void KeyPinky::collision()
{

    auto colliding_items = collidingItems();

    // if one of the colliding items is an Enemy, destroy both the bullet and the enemy
    foreach (auto item, colliding_items){

        if (typeid(*item) == typeid(PlayerPinky)){
            //decH=true;
            scene()->removeItem(this);
            qDebug()<<"kolizija igrac i enemy";
            return;

        }

        /*if(typeid(*item) == typeid(KeyPinky)){
            //incNumberOfKeys();
            if(getNumberOfLife()==0){
                //incSc=true;
                setScoreChange(1);
                setIsAlive(false);

                return;
            }
        }*/
    }

}
