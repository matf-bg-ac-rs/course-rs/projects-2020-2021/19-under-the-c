#include "Headers/Game.h"
#include <Headers/Button.h>
#include <QDebug>
#include <iostream>

Game::Game(QObject * parent):
    QObject(parent),
    current_level(0),
    timer(nullptr),
    numberOfLifes(3)
{

}
Game::~Game()
{
    delete timer;
    delete view;
}

void Game::addLevel(LevelScene * level)
{
    levels.append(level);
}

void Game::onPlay()
{
    auto level = levels.at(current_level);

    if(level == nullptr)
        return;

    level->initialize();

    view->scene()->clear();
    view->viewport()->update();
    view->setScene(level);


    view->show();

    timer = new QTimer();

    connectAll(level);
    connect(level, &LevelScene::center, this, &Game::onCenter);
    connect(level, &LevelScene::level, this, &Game::onLevel);
    timer->start(25);


    numberOfLifes = 3;

}

void Game::onPlayLevel(int chosen_level)
{

        current_level=chosen_level;
        auto level = levels.at(current_level);

        if(level == nullptr)
            return;

        level->initialize();

        view->scene()->clear();
        view->viewport()->update();
        view->setScene(level);



        view->show();

        timer = new QTimer();

        connectAll(level);
        connect(level, &LevelScene::center, this, &Game::onCenter);
        connect(level, &LevelScene::level, this, &Game::onLevel);
        timer->start(25);


        numberOfLifes = 3;

}

void Game::initializeMeniItems()
{   Button *startButton= new Button();

    startButton->initialize(":/images/buttons/start.png",0.2, 300,500);
    menu_scene->addItem(startButton);

    Button *exitButton=new Button();
    exitButton->initialize(":/images/buttons/exit.png", 0.2, 550,500);
    menu_scene->addItem(exitButton);

    menu_scene->initialize();

    Button *level1Button= new Button(0);
    level1Button->initialize(":/images/buttons/level1.png",0.2, 300,0);
    menu_scene->addItem(level1Button);

    Button *level2Button= new Button(1);
    level2Button->initialize(":/images/buttons/level2.png",0.2, 50,0);
    menu_scene->addItem(level2Button);

    Button *level3Button= new Button(2);
    level3Button->initialize(":/images/buttons/level3.png",0.2, 850,200);
    menu_scene->addItem(level3Button);

    Button *level4Button= new Button(3);
    level4Button->initialize(":/images/buttons/level4.png",0.2, 550,0);
    menu_scene->addItem(level4Button);



    Button::connect(startButton,SIGNAL(clicked()),this,SLOT(onPlay()));
    Button::connect(exitButton,SIGNAL(clicked()),this,SLOT(onExit()));
    Button::connect(level1Button,&Button::clicked_level, this, &Game::onPlayLevel);
    Button::connect(level2Button,&Button::clicked_level, this, &Game::onPlayLevel);
    Button::connect(level3Button,&Button::clicked_level, this, &Game::onPlayLevel);
    Button::connect(level4Button,&Button::clicked_level, this, &Game::onPlayLevel);


}
void Game::initialize()
{
    auto level1 = new LevelMimi();
    auto level2 = new LevelPlatform();
    auto level3 = new LevelMermaid();
    auto level4 = new LevelPinky();

    addLevel(level1);
    addLevel(level2);
    addLevel(level3);
    addLevel(level4);

    current_level = 0;

    view= new QGraphicsView();

    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setFixedSize(QSize(1280, 720));


    menu_scene = new MenuScene();
    this->view->setScene(menu_scene);
    this->view->show();
    initializeMeniItems();


}


void Game::onLifeLost()
{
    numberOfLifes--;

    if(numberOfLifes <= 0){
        timer->stop();
        view->scene()->clear();

        disconnectAll(levels.at(current_level));
        disconnect(levels.at(current_level), &LevelScene::center, this, &Game::onCenter);
        disconnect(levels.at(current_level), &LevelScene::level, this, &Game::onLevel);

        //return;
        view->setScene(menu_scene);
        initializeMeniItems();

        return;

    }
    else{
        timer->stop();
        view->scene()->clear();

        auto level = levels.at(current_level);

        level->initialize();
        view->viewport()->update();

        timer->start(25);
    }
}

void Game::onLifeCollected()
{
    numberOfLifes++;
}

void Game::onLevelCompleted()
{
    timer->stop();

    disconnectAll(levels.at(current_level));
    disconnect(levels.at(current_level), &LevelScene::center, this, &Game::onCenter);
    disconnect(levels.at(current_level), &LevelScene::level, this, &Game::onLevel);


    current_level++;


    auto level = levels.at(current_level);
    level->initialize();

    view->scene()->clear();
    view->viewport()->update();
    view->setScene(level);


    connectAll(level);
    connect(level, &LevelScene::center, this, &Game::onCenter);
    connect(level, &LevelScene::level, this, &Game::onLevel);


    timer->start(25);

}

void Game::onCenter(PlayerPlatform *player, Health* h, Score* s1, Score* s2, Rules* rules, Button *button, Button *buttonMute)
{
    view->centerOn(player);

    QPointF pozicija = view->mapToScene(10,20);
    h->setPos(pozicija.x(), pozicija.y());

    pozicija = view->mapToScene(10,40);
    s1->setPos(pozicija.x(), pozicija.y());

    pozicija = view->mapToScene(10,60);
    s2->setPos(pozicija.x(), pozicija.y());
    if(buttonMute){
        pozicija = view->mapToScene(20,90);
        buttonMute->setPos(pozicija.x(), pozicija.y());
    }
    pozicija = view->mapToScene(200,100);
    rules->setPos(pozicija.x(), pozicija.y());

    pozicija = view->mapToScene(500,500);
    button->setPos(pozicija.x(), pozicija.y());

}

void Game::onExit()
{
    QApplication::closeAllWindows();
}
void Game::onGamePaused()
{
    if(timer->isActive())
        timer->stop();
    else
        timer->start();

}

void Game::onGameOver()
{
    timer->stop();


    disconnectAll(levels.at(current_level));
    disconnect(levels.at(current_level), &LevelScene::center, this, &Game::onCenter);
    disconnect(levels.at(current_level), &LevelScene::level, this, &Game::onLevel);
    view->scene()->clear();
    view->setScene(menu_scene);
    current_level=0;
    initializeMeniItems();

}

void Game::onLevel(int brLevel)
{
    if(brLevel==current_level){
        timer->stop();
        view->scene()->clear();

        auto level = levels.at(current_level);

        level->initialize();
        view->viewport()->update();

        timer->start(25);
        return;
    }

    timer->stop();


    disconnectAll(levels.at(current_level));
    disconnect(levels.at(current_level), &LevelScene::center, this, &Game::onCenter);
    disconnect(levels.at(current_level), &LevelScene::level, this, &Game::onLevel);


    current_level=brLevel;


    auto level = levels.at(current_level);

    level->initialize();

    view->scene()->clear();
    view->viewport()->update();
    view->setScene(level);
    view->show();

    connectAll(level);
    connect(level, &LevelScene::center, this, &Game::onCenter);
    connect(level, &LevelScene::level, this, &Game::onLevel);


    timer->start(25);
}

void Game::disconnectAll(const QObject *sender)
{
    disconnect(timer, SIGNAL(timeout()),levels.at(current_level) , SLOT(update()));
    disconnect(sender, SIGNAL(lifeLost()), this, SLOT(onLifeLost()));
    disconnect(sender, SIGNAL(lifeCollected()), this, SLOT(onLifeCollected()));
    disconnect(sender, SIGNAL(levelCompleted()), this, SLOT(onLevelCompleted()));
    disconnect(sender, SIGNAL(gamePaused()), this, SLOT(onGamePaused()));
    disconnect(sender, SIGNAL(gameOver()), this, SLOT(onGameOver()));

}

void Game::connectAll(const QObject *level)
{
    connect(timer, SIGNAL(timeout()), level, SLOT(update()));
    connect(level, SIGNAL(lifeLost()), this, SLOT(onLifeLost()));
    connect(level, SIGNAL(lifeCollected()), this, SLOT(onLifeCollected()));
    connect(level, SIGNAL(levelCompleted()), this, SLOT(onLevelCompleted()));
    connect(level, SIGNAL(gamePaused()), this, SLOT(onGamePaused()));
    connect(level, SIGNAL(gameOver()), this, SLOT(onGameOver()));
}
