#include <QGraphicsScene>
#include "Headers/Star.h"
#include "QPainter"
#include <stdlib.h>
#include <QTimer>
#include <QDebug>
#include <Headers/PlayerPinky.h>
int Star::brojac=0;
Star::Star(QGraphicsItem *parent)
{
        setType(STAR);
        setIsAlive(true);
        setPixmap(QPixmap(":/images/collectables/star.png"));
        setScale(0.08);

        ID=brojac;
        brojac++;

}
