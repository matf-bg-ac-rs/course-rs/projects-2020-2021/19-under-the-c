#include "Headers/Music.h"
#include "Headers/Button.h"
extern Button musicButton;
bool Music::mute=false;
void Music::pushBackSound(const QString &url)
{
    QMediaPlayer *music = new QMediaPlayer();
    music->setMedia(QUrl(url));

    musics.push_back(music);
}

void Music::checkMute()
{
    if(mute){
        foreach(auto music, musics)
            music->setMuted(true);

    }else{
        foreach(auto music, musics)
            music->setMuted(false);

    }

}

void Music::setMute()
{
    if(!musics[0]->isMuted()){

        mute=true;
    }else{

        mute=false;
    }

}



void Music::makeSound(int indexOfElemt)
{
    if(size()==0)
        return;
    QMediaPlayer *music=musics[indexOfElemt];
    if (music->state() == QMediaPlayer::PlayingState){
            music->setPosition(0);
            }

    else if (music->state() == QMediaPlayer::StoppedState){
        music->play();
    }

}

void Music::deleteSounds()
{
    int n=musics.size();
    for(int i=0; i<n; i++)
        delete musics[i];

    musics.clear();



}

void Music::pauseSonuds()
{
 foreach(auto music, musics)
     if (music->state() == QMediaPlayer::PlayingState)
         music->pause();
}

void Music::stopSound(int indexOfElemt)
{
    if(size()<=indexOfElemt)
        return;
    musics[indexOfElemt]->stop();

}

void Music::playSounds()
{
    foreach(auto music, musics)
        if (music->state() == QMediaPlayer::PausedState)
            music->play();
}

void Music::playSound(int indexOfElemt)
{
    if(size()<=indexOfElemt)
        return;
    musics[indexOfElemt]->play();

}

int Music::size()
{
    return musics.size();
}
