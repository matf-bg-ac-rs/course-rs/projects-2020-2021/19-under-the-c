#include <QGraphicsScene>
#include "Headers/Treasure.h"
#include "QPainter"
#include <stdlib.h>
#include <QTimer>
#include <QDebug>
#include <Headers/PlayerPinky.h>
int Treasure::brojac=0;
Treasure::Treasure(QGraphicsItem *parent):ItemOfScene(parent)
{
        setType(TREASURE);
        setIsAlive(true);
        setPixmap(QPixmap(":/images/other/pinky_scene/treasureF.png"));
        setScale(0.2);

        ID=brojac;
        brojac++;

}
