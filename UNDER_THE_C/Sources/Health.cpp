#include "Headers/Health.h"
#include <iostream>

Health::Health(QGraphicsItem * parent):
    QGraphicsTextItem(parent)
{
    health=3;

    setPlainText(QString("Health: ") + QString::number(health));
    setDefaultTextColor(Qt::black);
    QGraphicsTextItem::setFont(QFont("times", 16));
}
/*
void Health::move(int player_x, int player_y){
    setPos(player_x-200, player_y-100);
}
*/
void Health::decrease(){
    health--;
    setPlainText(QString("Health: ") + QString::number(health));
}

int Health::getHealth(){
    return health;
}

void Health::setHealth(int value)
{//std::cout<<"ovde je"<<std::endl;
    health = value;
    setPlainText(QString("Health: ") + QString::number(health));
}

void Health::move(int player_x, int player_y)
{
    setPos(player_x - 100, player_y - 100);
}
