#include "Headers/Door.h"

Door::Door(QObject* parent)
{
    setMusicEmit(0);
}

Door::Door(char type)
{
    isAlive = true;
    setType(DOOR);

    this->type = type;

    switch (type){
    case 'W' :
        setPixmap( QPixmap(":/images/platforms/wall1.png"));
        setScale(1.3);
        break;
    case 'G' :
        setPixmap( QPixmap(":/images/platforms/wall2.png"));
        setScale(1.3);
        break;
    default:
        break;
    }
}

void Door::collision()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (PlayerPlatform)){

            auto player = (qgraphicsitem_cast<PlayerPlatform *>(colliding_items[i]));

            if(player->getCollectedCristals() == 5 && type == 'W'){
                setIsAlive(false);
                setMusicEmit(1);
            }
            if(player->getGoldenCollected() && type == 'G'){
                setIsAlive(false);
                setMusicEmit(1);
            }
        }
    }
}
