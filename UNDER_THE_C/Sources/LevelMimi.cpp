#include "Headers/LevelMimi.h"
#include <QDebug>
#include <QKeyEvent>
#include "Headers/Key.h"
#include "Headers/KillerFire.h"
#include <iostream>
#include <Headers/StraightMotion.h>
#include <Headers/Button.h>

unsigned int timeCounter =0;

LevelMimi::LevelMimi(QObject *parent):
    LevelScene(parent)
{}

void LevelMimi::initialize()
{
    timeCounter=0;

    musicButton= new Button();
    musicButton->initialize("://images/buttons/speaker.png", 0.03, 20, 50);
    addItem(musicButton);
    if(mute==true)
        musicButton->initialize("://images/buttons/mute.png", 0.03, 5, 55);

   connect(musicButton, SIGNAL(clicked()),this, SLOT(onSetMute()) );
    buttonRules=new Button();
    buttonRules->initialize(":/images/buttons/PinkyButton.png", 1, 500,500);
    connect(buttonRules, SIGNAL(clicked()),this, SLOT(onClicked()) );

    player = new PlayerMimi();
    addItem(player);


    setFocus(Qt::TabFocusReason);

    player->setPos(500, 50);

    pScore = new Score();
    addItem(pScore);

    pHealth = new Health();
    pHealth->setPos(pHealth->x(), pHealth->y()+25);
    addItem(pHealth);

    MimisWings=new WingsScore();
    MimisWings->setPos(MimisWings->x()+430, MimisWings->y());
    addItem(MimisWings);
    addImage(new WingsMini(),":/images/collectables/wing-clipart-5.png", MimisWings->x()-40,MimisWings->y(), 0.2);

    time=new Timer();
    time->setPos(time->x()+800, time->y());
    addItem(time);

    setBackgroundBrush(QPixmap(":/images/backgrounds/download.png"));
    setSceneRect(0,0,1000,700);

    speed_items=0;
    stop=false;
    stopEventKey=false;
    moveLeft=false ;
    moveRight=false;
    pause=false;
    moveItems=true;
    onRules=false;
    notice=new TextOfNotice();




    //inicijalizacija igraca koji treba biti spasen
    koko=new PlayerPlatform();
    addItem(koko);
    koko->setPos(width()-150, height()+(koko->boundingRect().height())/10);
    //kavez za zarobljenika
    cage= new QGraphicsPixmapItem();
    addImage(cage, ":/images/other/mimi_scene/cage-clipart-transparent-background.png",
             width()-30-150, height()+(koko->boundingRect().height())/10-50,0.6);

    photoStopwatch= new QGraphicsPixmapItem();
    addImage(photoStopwatch,":/images/other/mimi_scene/see-clipart-blank-watch-5.png", photoStopwatch-> x()+780,photoStopwatch->y(), 0.1);


    liana= new QGraphicsPixmapItem();
    addImage(liana, ":/images/other/mimi_scene/liana.png",750,-525, 0.215);


    pushBackSound("qrc:/sounds/POL-change-short.wav");
    pushBackSound("qrc:/sounds/fire.mp3");
    checkMute();

}

void LevelMimi::onSetMute()
{

    setMute();
    if(mute==true)
        musicButton->initialize("://images/buttons/mute.png", 0.03, 5, 55);
    else
        musicButton->initialize("://images/buttons/speaker.png", 0.03, 5, 55);
    checkMute();
}

void LevelMimi::update()
{

    if(moveItems)
        player->image_change();


    // CREATE CLOUDS
    if(timeCounter % 10==0){
        Cloud *cloud= new Cloud();
        addItem(cloud);
        items.insert(cloud->getID(), cloud);
        cloud->incID();

    }

    // CREATE ENEMY
    if(timeCounter%10==0 && !stop){

        if(timeCounter<360){
        Enemy *enemy= new Enemy(1);
        addItem(enemy);
        items.insert(enemy->getID(), enemy);
        enemy->incID();
    }else if(timeCounter==720){

           ItemOfScene *spec= new KillerEnemy(2);
           addItem(spec);
           spec->setPos(200, 700);
           items.insert(spec->getID(), spec);
           indexEnemy=spec->getID();
           spec->incID();


           spec= new KillerEnemy(2);
           addItem(spec);
           spec->setPos(450, 700);
           items.insert(spec->getID(), spec);
           spec->incID();

           spec= new KillerEnemy(2);
           addItem(spec);
           spec->setPos(700, 700);
           items.insert(spec->getID(), spec);
           spec->incID();

        }
        else{
        Enemy *enemy= new Enemy(2);
        addItem(enemy);
        items.insert(enemy->getID(), enemy);
        enemy->incID();

        }


    }

    if(timeCounter==360){
        notice->setText("Enemy have 2 life");
        notice->setDuration(1);
        notice->ItemOnScene(true);
        addItem(notice);
    }
    // CREATE KEYS
    if(timeCounter % 40==0 && !stop){
        Key *key= new Key();
        addItem(key);
        items.insert(key->getID(),key);
        key->incID();
        key->setType(ItemOfScene::typeOfItem::KEY);
        speed_items=speed_items+0.35;


    }

    //CREATE BALLOON
    if(timeCounter==600){
        Balloon *balloon=new Balloon();
        addItem(balloon);
        items.insert(balloon->getID(), balloon);
        balloon->incID();


    }

    if(moveLeft){
        player->moveLeft();
    }

    if(moveRight){
        player->moveRight();
    }



    // CREATE WINGS
    if(timeCounter%280==0 && !stop){
        WingsMini *wings=new WingsMini();
        addItem(wings);
        items.insert(wings->getID(), wings);
        wings->incID();
    }

    //CREATE KILLERFIRE
    if(timeCounter>=720 && timeCounter%20==0){
        auto item=items.find(indexEnemy);
        if(item!=items.end()){
            ItemOfScene *kill=new KillerFire(item.value()->x() ,item.value()->y());
            addItem(kill);
            items.insert(kill->getID(), kill);
            kill->incID();
        }
        item=items.find(indexEnemy+1);
        if(item!=items.end()){
            ItemOfScene *kill=new KillerFire(item.value()->x() ,item.value()->y());
            addItem(kill);
            items.insert(kill->getID(), kill);
            kill->incID();
        }
        item=items.find(indexEnemy+2);
        if(item!=items.end()){
            ItemOfScene *kill=new KillerFire(item.value()->x() ,item.value()->y());
            addItem(kill);
            items.insert(kill->getID(), kill);
            kill->incID();
        }

    }

    //Stopwatch
    if(timeCounter%20==0){
        time->nextSecond();
    }





    // BECKGROUND MUSIC
    if(timeCounter %300==0 || timeCounter==1){
        makeSound(0);
    }




    if(!items.isEmpty())

        for(auto it=items.begin(); it!=items.end(); it++){


                if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMY){
                    auto enemy = (qgraphicsitem_cast<Enemy *>(it.value()));

                    enemy->move(speed_items);
                    enemy->collision();

                    if(enemy->getScoreChange() > 0){
                        pScore->increase();
                        enemy->setScoreChange(0);
                    }

                    if(enemy->getHealthChange() > 0){
                        pHealth->decrease();
                        if(pHealth->getHealth() <= 0){
                           clean_mem();
                           emit lifeLost();
                           return;
                        }
                    }
                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::CLOUD && moveItems){
                    auto cloud = (qgraphicsitem_cast<Cloud *>(it.value()));
                    cloud->move(speed_items);
                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::FIRE){
                    auto fire = (qgraphicsitem_cast<Fire *>(it.value()));
                    fire->move(0, 10, height());
                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::KILLERENEMY){
                    auto killer = (qgraphicsitem_cast<KillerEnemy *>(it.value()));
                    killer->move();
                    killer->collision();

                    if(killer->getScoreChange() > 0){
                        pScore->increase();
                        killer->setScoreChange(0);
                    }
                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::KILLERFIRE){
                    auto kill = (qgraphicsitem_cast<KillerFire *>(it.value()));
                    kill->move(speed_items);
                    kill->collision();

                    if(kill->getHealthChange() > 0){
                        pHealth->decrease();
                        if(pHealth->getHealth() <= 0){
                           clean_mem();
                           emit lifeLost();
                           return;
                        }
                    }
                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::KEY){
                    auto key = (qgraphicsitem_cast<Key *>(it.value()));
                    key->move(speed_items);
                    key->collision();

                    if(key->getScoreChange() > 0){
                        pScore->increase();
                        pScore->increase();
                        key->setScoreChange(0);
                    }

                }
                else if(it.value()->getType() == ItemOfScene::typeOfItem::WINGS){
                    auto wing = (qgraphicsitem_cast<WingsMini *>(it.value()));
                    wing->move(speed_items);
                    wing->collision();

                    if(wing->getWingsChange())
                        MimisWings->inc_number_of_wings();

                }else if(it.value()->getType() == ItemOfScene::typeOfItem::BALLOON){
                    auto ballon=(qgraphicsitem_cast<Balloon*>(it.value()));
                    ballon->move(speed_items);
                    if(ballon->incLife()){
                        notice->setText("Life collected");
                        notice->setDuration(1);
                        notice->ItemOnScene(true);
                        addItem(notice);
                        emit lifeCollected();
                    }


                }



}




    if(notice->IsOnScene()){
        if(notice->getDuration()==0){

            removeItem(notice);
            notice->ItemOnScene(false);
        }

    }

 int key_check = -1;
    if(!items.isEmpty()){
        for(auto it=items.begin(); it!=items.end(); it++){
            if(key_check != -1){

                           auto values=items.take(key_check);
                           removeItem(values);
                           delete values;

                           key_check = -1;
                       }

            if(it.value()->getIsAlive() == false){

                key_check=it.key();

            }
        }
    }

    if(key_check != -1){
            auto values=items.take(key_check);
            removeItem(values);
            delete values;

            key_check = -1;
        }





    end_animation(1201);

    timeCounter++;
}






void LevelMimi::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_P && !onRules){
       emit gamePaused();
        if(!pause ){
            stopEventKey=true;
            pauseSonuds();
        }else{
            stopEventKey=false;
            playSounds();

        }
        pause=!pause;

    }else if(event->key()==Qt::Key_M){

       setMute();
       if(mute==true)
           musicButton->initialize("://images/buttons/mute.png", 0.03, 5, 55);
       else
           musicButton->initialize("://images/buttons/speaker.png", 0.03, 5, 55);
       checkMute();
    }
    if(event->key()==Qt::Key_B){
        clean_mem();
        emit gameOver();
    }
    if(event->key()==Qt::Key_1){
        clean_mem();
        emit level(0);
        return;
    }
    if(event->key()==Qt::Key_2){
        clean_mem();
        emit level(1);
    }
    if(event->key()==Qt::Key_3){
        clean_mem();
        emit level(2);
    }
    if(event->key()==Qt::Key_4){
        clean_mem();
        emit level(3);
    }

    if(event->isAutoRepeat() == false && event->key()==Qt::Key_H && !onRules){

        if(!pause){

            emit gamePaused();
            stopEventKey=true;
            pauseSonuds();
            pause=!pause;
        }
        rules->setNumberOfLevel(1);
        buttonRules->setZValue(10);
        addItem(rules);
        addItem(buttonRules);
        onRules=true;


    }
    if(stopEventKey)
        return;

    if(event->key()==Qt::Key_Left){
        moveLeft=true;

    }
    else if(event->key()==Qt::Key_Right){

        moveRight=true;
    }

    else if(event->isAutoRepeat() == false && event->key()==Qt::Key_Space){
        fire=new Fire(player->x(),player->y()+70);
        addItem(fire);
        items.insert(fire->getID(), fire);
        fire->incID();
        makeSound(1);

    }else if(event->key()==Qt::Key_C){
        if(MimisWings->use_wings())
            speed_items=0;


    }


}

void LevelMimi::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left){
        moveLeft=false;


    }

    if(event->key() == Qt::Key_Right){
        moveRight=false;
    }

    if(event->key()==Qt::Key_Space){


    }
}


void LevelMimi::clean_mem(){

    for(auto cc=items.begin(); cc != items.end(); cc++){

        delete cc.value();
}
    items.clear();


    deleteSounds();
    if(onRules)
        removeItem(rules);
    delete buttonRules;
    delete musicButton;
    delete player;
    delete notice;
    delete time;
    delete koko;
    delete MimisWings;

}


void LevelMimi::end_animation(unsigned int game_duraton){

    if(timeCounter==game_duraton-50){
        stop=true;

        auto item=items.find(indexEnemy);
        if(item!=items.end()){
             qgraphicsitem_cast<KillerEnemy*>(item.value())->setEndOfGame(true);

        }
        item=items.find(indexEnemy+1);
        if(item!=items.end()){
            qgraphicsitem_cast<KillerEnemy*>(item.value())->setEndOfGame(true);

        }

        item=items.find(indexEnemy+2);
        if(item!=items.end()){
            qgraphicsitem_cast<KillerEnemy*>(item.value())->setEndOfGame(true);
        }

    }

    if(timeCounter>=game_duraton){
        stopEventKey=true;
        if(koko->pos().y()>550){
            koko->setPos(koko->x(),koko-> y()-10);
            cage->setPos(cage->x(), cage->y()-10);
        }

            if(timeCounter==1201){
                if(pScore->getScore()>=70){

                    notice->setText("Level Completed");
                    addItem(notice);
                }else{

                    notice->setText("Life Lost");
                    addItem(notice);
                }
            }

        if(player->pos().y()+(player->boundingRect().height()/10) <height()){
            player->setPos(player->x(),player-> y()+10);

        }else if(pScore->getScore()<70){
            clean_mem();
            emit lifeLost();
            return;

        }else if(player->pos().x()< 710){
               moveItems=false;
            player->setPos(player->x()+10, player->y());

        }else if(liana->y()<0 && cage->y()==499.9){
            liana->setY(liana->y()+10);
        }else if(cage->y()>cage->boundingRect().height()*-0.6){
            cage->setY(cage->y()-10);
            liana->setY(liana->y()-10);

        }else if(pScore->getScore()>=70){
                clean_mem();
                emit levelCompleted();
            }


    }

}

void LevelMimi::addImage(QGraphicsPixmapItem *item, const QString &fileName, qreal ax, qreal ay, qreal scale)
{
    item->setPixmap(QPixmap(fileName));
    item->setScale(scale);
    item->setPos(ax, ay);
    addItem(item);
}

void LevelMimi::onClicked(){
    removeItem(rules);
    removeItem(buttonRules);
    pause=!pause;
    emit gamePaused();
    playSounds();
    stopEventKey=false;
    onRules=false;
}
