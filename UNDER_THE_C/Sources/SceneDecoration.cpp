#include "Headers/SceneDecoration.h"

SceneDecoration::SceneDecoration()
{

}

SceneDecoration::SceneDecoration(char type, int x, int y)
{
    switch (type) {
    case 'r' :
        setPixmap(QPixmap(":/images/other/koko_scene/rock1.png"));
        //setScale(0.85);
        setPos(x*80,y*70);
        setZValue(1);
        break;
    case 't' :
        setPixmap(QPixmap(":/images/other/koko_scene/rock3.png"));
        setScale(0.7);
        setPos(x*80,y*70-10);
        setZValue(1);
        break;
    case 'l' :
        setPixmap(QPixmap(":/images/other/koko_scene/log1.png"));
        setScale(0.5);
        setPos(x*80,y*70-50);
        setZValue(1);
        break;
    case 'k' :
        setPixmap(QPixmap(":/images/other/koko_scene/log2.png"));
        setScale(0.5);
        setPos(x*80,y*70-50);
        setZValue(1);
        break;
    case ';' :
        setPixmap(QPixmap(":/images/other/koko_scene/log3.png"));
        setScale(0.5);
        setPos(x*80,y*70-50);
        setZValue(1);
        break;
    case 'o' :
        setPixmap(QPixmap(":/images/other/koko_scene/bush.png"));
        setScale(0.8);
        setPos(x*80,y*70-50);
        setZValue(1);
        break;
    case 'g' :
        setPixmap(QPixmap(":/images/other/koko_scene/grass4.png"));
        setPos(x*80,y*70+30);
        setZValue(-1);
        break;
    case 'h' :
        setPixmap(QPixmap(":/images/other/koko_scene/grass8.png"));
        setScale(0.8);
        setPos(x*80,y*70+20);
        setZValue(1);
        break;
    case 'j' :
        setPixmap(QPixmap(":/images/other/koko_scene/grass6.png"));
        //setScale(0.85);
        setPos(x*80,y*70+20);
        setZValue(1);
        break;
    case 'd' :
        setPixmap(QPixmap(":/images/other/koko_scene/tree.png"));
        //setScale(0.85);
        setPos(x*80,y*70);
        setZValue(-1);
        break;
    case 'f' :
        setPixmap(QPixmap(":/images/other/koko_scene/tree2.png"));
        setScale(0.8);
        setPos(x*80,y*70);
        setZValue(-1);
        break;
    case 'M' :
        setPixmap(QPixmap(":/images/other/koko_scene/mermaid_caught.png"));
        setScale(0.4);
        setPos(x*80,y*70);
        setZValue(-1);
        break;
    }
}
