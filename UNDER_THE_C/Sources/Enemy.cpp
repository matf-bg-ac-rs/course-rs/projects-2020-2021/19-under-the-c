#include "Headers/Enemy.h"
#include <QTimer>
#include <QDebug>
#include <QGraphicsScene>
#include <stdlib.h>
#include <QDebug>
#include "Headers/Health.h"
#include <Headers/PlayerMimi.h>
#include <Headers/StraightMotion.h>
#include <iostream>





int Enemy::brojac = 0;
double Enemy::ubrzanje_enemy=1;
Enemy::Enemy(int numberOfLife,StraightMotion *parent):StraightMotion(parent)
{

    setIsAlive(true);
    setType(ENEMY);

    int random=rand()%700;
    setPixmap(QPixmap(":/images/enemies/crow.png"));
    setScale(0.1);
    setPos(random, 700);

    health = numberOfLife;
    health_change = 0;
    score_change = 0;
    dx = -10;
    dx1 = 10;
    direction=true;
    sizeOfPath=0;

    // WARNING: Ovo se vise ne koristi u zasebnim klasama vec samo u itemOfScene
    // ali moze da se izbaci tek kad se izbaci iz Pinky nivoa
    ID = brojac;
    brojac++;
}
int Enemy::getNumberOfLife()const{
    return health;

}
void Enemy::incNumberOfLife(){
    health--;
}



void Enemy::collision()
{

    auto colliding_items = collidingItems();


    foreach (auto item, colliding_items){

        if (typeid(*item) == typeid(PlayerMimi)){

            setHealthChange(1);

            setIsAlive(false);
            return;

        }

        if(typeid(*item) == typeid(Fire)){
            qgraphicsitem_cast<Fire *>(item)->setIsAlive(false);
            incNumberOfLife();
            if(getNumberOfLife()==0){
                //incSc=true;
                setScoreChange(1);
                setIsAlive(false);

                return;
            }
        }
    }

}

int Enemy::getDX(){
    return dx;
}

void Enemy::setDX(int value){
    dx = value;
}

int Enemy::getDX1(){
    return dx1;
}

void Enemy::setDX1(int value){
    dx1 = value;
}


