#include <Headers/Shell.h>
#include <QGraphicsScene>
#include <stdlib.h>
#include <QDebug>
#include "Headers/Health.h"
#include "Headers/Score.h"
#include "Headers/Game.h"
#include<QString>
#include <Headers/Bubble.h>
#include <iostream>

int Shell::counter=0;

Shell::Shell(QGraphicsPixmapItem *parent):StraightMotion(parent)
{
    setIsAlive(true);
    setType(SHELL);

    int random=rand()%720;
    setPos(1280,random);
    setPixmap(QPixmap(":/images/collectables/shells/shell.png"));
    setScale(0.09);
//    ID=counter;
//    counter++;

    opened=false;
    score_change = 0;

}


void Shell::collision()
{
    if(getIsAlive()==false)
        return;

    auto colliding_items= collidingItems();

    foreach(auto item, colliding_items){
        if(typeid (*item)== typeid(PlayerMermaid) && opened==true ){

            setIsAlive(false);
            setScoreChange(1);
            return;
        }
        else if(typeid (*item)== typeid(Bubble) && opened == false){
            opened=true;
            qgraphicsitem_cast<Bubble *>(item)->setIsAlive(false);
            setPixmap(QPixmap(":/images/collectables/shells/pearl1.png"));

        }
    }
}

bool Shell::getOpened()
{
    return opened;
}
