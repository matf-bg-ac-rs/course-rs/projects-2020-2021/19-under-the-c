#include "Headers/KillerEnemy.h"

KillerEnemy::KillerEnemy(int numberOfLife, QGraphicsItem *parent):ItemOfScene(parent)
{
    setType(KILLERENEMY);
    setIsAlive(true);

    health = numberOfLife;
    health_change = 0;
    // NOTE: Ako se ovaj neprijatelj racuna  u score staviti da nasledjuje EnemiesWithScore
    //score_change = 0;
    //Sliku sam ja prekopirala jer je nije imao
    setPixmap(QPixmap(":/images/enemies/crow.png"));
    setScale(0.1);

    direction=true;
    sizeOfPath=0;
    endMove=false;
}

void KillerEnemy::setEndOfGame(bool value)
{
    endMove=value;
}

void KillerEnemy::collision()
{
    auto colliding_items = collidingItems();


    foreach (auto item, colliding_items){

        if(typeid(*item) == typeid(Fire)){
            qgraphicsitem_cast<Fire *>(item)->setIsAlive(false);

            health--;
            if(health==0){

                setScoreChange(1);
                setIsAlive(false);

                return;
            }
        }
    }
}

void KillerEnemy::move()
{
    if(y()>600 || endMove){
        setY(y()-10);
    }else{

    if(direction){
        setPos(x()-10, y());
        sizeOfPath+=10;
        if(sizeOfPath==200){
            direction=!direction;
            sizeOfPath=0;
        }

    }else{
        setPos(x()+10,y());
        sizeOfPath+=10;
        if(sizeOfPath==200){
            direction=!direction;
            sizeOfPath=0;
        }
    }

}
  // collision();

}

