#include <Headers/DoorFinish.h>

#include <QGraphicsScene>
#include "QPainter"
#include <stdlib.h>
#include <QTimer>
#include <QDebug>
#include "Headers/PlayerPinky.h"

int DoorFinish::brojac=0;
DoorFinish::DoorFinish(QGraphicsItem *parent)
{
        setType(DOORF);
        setIsAlive(true);
        setPixmap(QPixmap(":/images/other/pinky_scene/dollars_preview_rev_1.png"));
        setScale(0.75);
        ID=brojac;
        brojac++;
}
