#include "Headers/LevelPinky.h"
#include <QDebug>
#include "Headers/KeyPinky.h"
#include "Headers/Star.h"
#include <QKeyEvent>
#include <Headers/Stars.h>
#include <Headers/DoorPinky.h>
#include <Headers/Treasure.h>
#include <Headers/EnemyL.h>
#include <Headers/StarsL.h>
#include <Headers/DoorFinish.h>
#include <Headers/EnemyFly.h>
// Dodavanje drugih klasa pisati u .h fajl
#include <iostream>
//WARNING: Globalne promenljive su samo privremene i bice dodate u klasu po potrebi
int radi = 0;
LevelPinky::LevelPinky(QObject *parent):
    LevelScene(parent)
{

}

void LevelPinky::addPlatform(int x, int y, int width, int height){
    Platform* p = new Platform();
        p->setRect(0, 0, width, height);
        //p->setPixmap(QPixmap(":/images/platforms/k2.png"));
        //    p->setScale(0.05);
        p->setPos(x*50, y*50);
        //QPainter* painter = new QPainter();
        //QStyleOptionGraphicsItem* option = QStyleOptionGraphicsItem::Type;
       // p->paint(painter, QStyleOptionGraphicsItem::Type, 0 );

        addItem(p);
        platforme.push_back(p);

}
//Ovde se pise opis scene i njen STATICNI izlged(velicina scene, raspored platformi...)
void LevelPinky::initialize(){
    pause=false;
    stopEventKey=false;
    endAnimation=false;
    onRules=false;

    musicButton= new Button();
    musicButton->initialize(":/images/buttons/speakerWhite.png", 0.03, 20, 50);
    addItem(musicButton);
    connect(musicButton, SIGNAL(clicked()),this, SLOT(onSetMute()) );
    if(mute==true)
        musicButton->initialize(":/images/buttons/muteWhite.png", 0.03, 5, 55);


    buttonRules=new Button();
    buttonRules->initialize(":/images/buttons/PinkyButton.png", 1, 500,500);
    connect(buttonRules, SIGNAL(clicked()),this, SLOT(onClicked()) );
    setBackgroundBrush(QPixmap(":/images/backgrounds/background_5.png").scaled(1500, 1500));
    //5750x2000 ceo ekran

    //setSceneRect(0,0,2000,1000);

    QString filename;
        filename = ":/sceneDesignPinky.txt";

        // Otvaranje datoteke sa nivoom
        QFile file(filename);

        if(!file.exists()){
            std::cout << "Fajl ne postoji!" << std::endl;
            return ;
        }
        if(!file.open(QIODevice::ReadOnly)){
            std::cout << "Nije uspelo otvaranje fajla!" << std::endl;
            return;
        }

        // Citanje podataka
        QTextStream in(&file);

        // U prvoj liniji su dimenzije matrice
        QStringList line = in.readLine().split(" ");
        int sizeX = line[0].toInt();
        int sizeY = line[1].toInt();

        // 50x50
        setSceneRect(0, 0, 50*(sizeX), sizeY*50);

        for(int y=1; y<sizeY; y++){
            QString linija = in.readLine();

            /*int platform_x =0;
            int platform_y = y;
            int width=0;
            int height=0;*/

            for(int x = 0; x<sizeX; x++){

                auto polje = linija[x];//.toLatin1();

                /*if(polje.isDigit()){

                    // 0 oznacava jedinicnu platformu
                    if(polje.digitValue() == 0){
                        addPlatform(x, platform_y, 1 ,1);
                    }
                    height = polje.digitValue();
                    platform_x = x;
                }
                else if(polje == ';'){
                    addPlatform(platform_x, platform_y, ++width, height);
                    height = 0;
                    width = 0;
                }*/
                if(polje == 'x'){
                    addPlatform(x,y, 50, 50);
                }
                else if(polje == 'p'){
                    player = new PlayerPinky();
                    addItem(player);
                    setFocus(Qt::TabFocusReason);
                    player->setFocus();
                    player->setPos(x*50, y*50);
                }else if(polje == 'k'){
                    KeyPinky *key= new KeyPinky();
                    addItem(key);
                    key->setPos(x*50, y*50);
                    items.insert(key->getID(),key);
                    key->incID();
                }else if(polje == 'z'){
                    Star *star = new Star();
                    addItem(star);
                    star->setPos(x*50, y*50);
                    items.insert(star->getID(), star);
                    star->incID();
                }else if(polje == 't'){
                    Treasure *tr = new Treasure();
                    addItem(tr);
                    tr->setPos(x*50-20, y*50+25);
                    tr->setZValue(-1);
                    items.insert(tr->getID(), tr);
                    tr->incID();
                }else if(polje == 'd'){
                    DoorPinky *door = new DoorPinky();
                    addItem(door);
                    door->setPos(x*50-5, y*50-10);
                    items.insert(door->getID(), door);
                    door->incID();
                }else if(polje == 'f'){
                    DoorFinish *doorF = new DoorFinish();
                    addItem(doorF);
                    doorF->setPos(x*50, y*50);
                    items.insert(doorF->getID(), doorF);
                    doorF->incID();
                }else if(polje == 'e'){
                    EnemyFly* e = new EnemyFly();
                    e->setPos(x*50,y*50);
                    addItem(e);
                    items.insert(e->getID(), e);
                    e->incID();
                }else if(polje == 's'){
                    Treasure* web = new Treasure();
                    web->setPos(x*50,y*50);
                    addItem(web);
                    web->setPixmap(QPixmap(":/images/other/pinky_scene/hole-removebg-preview.png"));
                    web->setScale(0.2);
                    web->setZValue(-1);
                    items.insert(web->getID(), web);
                    web->incID();
                }

                /*else if(polje == 'f'){
                    Treasure *fr = new Treasure();
                    addItem(fr);
                    fr->setPixmap(QPixmap(":/images/other/pinky_scene/fireworks.png"));
                    fr->setPos(x*50, y*50);
                    fireworks.insert(fr->ID, fr);
                }*/


                /*if(height != 0){
                    width++;
                }*/
            }
        }



 /*   player = new PlayerPinky();
    addItem(player);
    setFocus(Qt::TabFocusReason);
    player->setFocus();
    player->setPos(500, 50);*/

    /*pScore = new Score(0);
    pScore->setDefaultTextColor(Qt::green);
    addItem(pScore);*/

    sScore = new Score(2);
    addItem(sScore);

    kScore = new Score(3);
    addItem(kScore);

    pHealth = new Health();
    pHealth->setHealth(5);
    pHealth->setDefaultTextColor(Qt::green);
    pHealth->setPos(pHealth->x(), pHealth->y()+25);
    addItem(pHealth);

    radi = 0;
    music=0;

    pushBackSound("qrc:/sounds/Dramatic Music - Castle Of Darkness (Mysterious Orchestral).mp3");
    checkMute();
    makeSound(0);
}
//Ovde se pise sta se pomera na tajmer(neprijatelji i slicno)
void LevelPinky::update()
{
    if(music==2400){
        makeSound(0);
        music=0;
    }
    music++;



    pHealth->move(player->x(), player->y());
    sScore->move(player->x(), player->y()+25);
    kScore->move(player->x(), player->y()+50);

    if(radi == 50){
        Enemy *enemy= new Enemy();
        addItem(enemy);
        enemy->setPixmap(QPixmap(":/images/enemies/spider.png"));
        enemy->setPos(3600, 1880);
        items.insert(enemy->getID(), enemy);
        enemy->incID();

        EnemyL *enemy1= new EnemyL();
        addItem(enemy1);
        //enemy1->setPixmap(QPixmap(":/images/enemies/spiderr.png"));
        //enemy1->setPos(0, 1880);
        items.insert(enemy1->getID(), enemy1);
        enemy1->incID();

        radi = 0;
    }
    radi++;
    if(!items.isEmpty()){
        for(auto it=items.begin(); it!=items.end(); it++){

            if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMY){
                auto enemy = (qgraphicsitem_cast<Enemy*>(it.value()));
                /*if(it.value()->pos().x() <= 3600){
                    enemy->setPixmap(QPixmap(":/images/enemies/spider.png"));
                    it.value()->setDX(10);
                }*/
                it.value()->setPos(it.value()->x()-it.value()->getDX(), it.value()->y());
                //provera da li je izasao van granica ekana
                if(it.value()->pos().x() >= 7000){
                    removeItem(it.value());
                    it.value()->setIsAlive(false);
                    //NOTE:take brise element iz mape i vaca vrednost
                    //auto value=items.take(it.key());
                    //delete value;
                }
                else{
                    auto collding_items=this->collidingItems(it.value());
                    for( int i=0, n=collding_items.size(); i<n; i++){
                        if(typeid (*(collding_items[i]))==typeid (PlayerPinky)){

                            pHealth->decrease();

                            //Ovde proveravam health kako bih videla da li igrac izgubio zivot
                            //ako jeste onda emitujem signal klasi Game da je zivot izgubljen
                            if(pHealth->getHealth() <= 0){

                                DeleteLevelPinky();
                                emit lifeLost();

                                return;
                            }

                            this->removeItem(it.value());
                            it.value()->setIsAlive(false);
                            //auto value=items.take(it.key());
                            //delete value;
                        }
                    }
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMYL){
                auto enemy = (qgraphicsitem_cast<EnemyL*>(it.value()));
                /*if(it.value()->pos().x() >= 2900){
                    enemy->setPixmap(QPixmap(":/images/enemies/spiderr.png"));
                    it.value()->setDX1(-10);
                }*/
                it.value()->setPos(it.value()->x()-it.value()->getDX1(), it.value()->y());
                //it.value()->pos().x() + it.value()->boundingRect().width()*0.2>7000
                //provera da li je izasao van granica ekana
                if(it.value()->pos().x() <= 0){
                    removeItem(it.value());
                    it.value()->setIsAlive(false);
                    //NOTE:take brise element iz mape i vaca vrednost
                    //auto value=items.take(it.key());
                    //delete value;
                }
                else{
                    auto collding_items=this->collidingItems(it.value());
                    for( int i=0, n=collding_items.size(); i<n; i++){
                        if(typeid (*(collding_items[i]))==typeid (PlayerPinky)){

                            pHealth->decrease();

                            //Ovde proveravam health kako bih videla da li igrac izgubio zivot
                            //ako jeste onda emitujem signal klasi Game da je zivot izgubljen
                            if(pHealth->getHealth() <= 0){

                                DeleteLevelPinky();
                                emit lifeLost();

                                return;
                            }

                            this->removeItem(it.value());
                            it.value()->setIsAlive(false);
//                            auto value=items.take(it.key());
//                            delete value;
                        }
                    }
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::STARS){
                it.value()->setPos(it.value()->x()+20, it.value()->y());

                //provera da li je izasao van granica ekana
                if(it.value()->pos().x() >= 7000){

                    removeItem(it.value());
                    it.value()->setIsAlive(false);
                    //NOTE:take brise element iz mape i vaca vrednost
                    //auto value=items.take(it.key());
                    //delete value;
                }else{


                    //inace proveravamo da li se sudario
                    auto collding_items=this->collidingItems(it.value());

                    foreach(QGraphicsItem *item, collding_items){
                        if(typeid (*item)==typeid (Enemy)){

                            this->removeItem(item);
                            this->removeItem(it.value());
                            auto enemy=(qgraphicsitem_cast<Enemy *>(item));
                            enemy->setIsAlive(false);
                            it.value()->setIsAlive(false);
                            //auto value1=items.take(id);
                            //delete value1;
                            //auto value2=items.take(it.key());
                            //delete value2;
                        }
                        if(typeid (*item)==typeid (EnemyL)){

                            //pScore->increase();
                            this->removeItem(item);
                            this->removeItem(it.value());
                            auto enemy=(qgraphicsitem_cast<EnemyL *>(item));
                            enemy->setIsAlive(false);
                            it.value()->setIsAlive(false);
                            //auto value1=items.take(id);
                            //delete value1;
                            //auto value2=items.take(it.key());
                            //delete value2;


                        }
                    }
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::STARSL){
                it.value()->setPos(it.value()->x()-20, it.value()->y());

                //provera da li je izasao van granica ekana
                if(it.value()->pos().x() <= 0){

                    removeItem(it.value());
                    it.value()->setIsAlive(false);
                    //NOTE:take brise element iz mape i vaca vrednost
                    //auto value=items.take(it.key());
                    //delete value;
                }else{


                    //inace proveravamo da li se sudario
                    auto collding_items=this->collidingItems(it.value());

                    foreach(QGraphicsItem *item, collding_items){
                        if(typeid (*item)==typeid (Enemy)){

                            this->removeItem(item);
                            this->removeItem(it.value());
                            auto enemy=(qgraphicsitem_cast<Enemy *>(item));
                            enemy->setIsAlive(false);
                            it.value()->setIsAlive(false);
                            //auto value1=items.take(id);
                            //delete value1;
                            //auto value2=items.take(it.key());
                            //delete value2;


                        }
                        if(typeid (*item)==typeid (EnemyL)){

                            this->removeItem(item);
                            this->removeItem(it.value());
                            auto enemy=(qgraphicsitem_cast<EnemyL *>(item));
                            enemy->setIsAlive(false);
                            it.value()->setIsAlive(false);
                            //auto value1=items.take(id);
                            //delete value1;
                            //auto value2=items.take(it.key());
                            //delete value2;


                        }
                    }
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::KEYP){

                    // proveravamo da li se sudario
                    auto colliding_items=this->collidingItems(it.value());

                    foreach(QGraphicsItem *item, colliding_items){
                        if(typeid (*item) == typeid (PlayerPinky)){

                            player->num_of_keys++;
                            kScore->increase();
                            this->removeItem(it.value());
                            it.value()->setIsAlive(false);
                            //auto value=items.take(it.key());
                            //delete value;
                        }
                    }
                //}
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::STAR){
                    //inace proveravamo da li se sudario
                    auto collding_items=this->collidingItems(it.value());

                    foreach(QGraphicsItem *item, collding_items){
                        if(typeid (*item) == typeid (PlayerPinky)){
                            player->num_of_stars++;
                            sScore->increase();
                            this->removeItem(it.value());
                            it.value()->setIsAlive(false);
                            //auto value=items.take(it.key());
                            //delete value;
                        }
                    }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::DOORP){
                auto collding_items=this->collidingItems(it.value());
                foreach(auto item, collding_items){
                    if(typeid (*item) == typeid (PlayerPinky)){
                        auto player = (qgraphicsitem_cast<PlayerPinky*>(item));
                        if(player->num_of_keys >= 1){
                            player->num_of_keys--;
                            kScore->decrease();
                            this->removeItem(it.value());
                            it.value()->setIsAlive(false);
                            //auto value=items.take(it.key());
                            //delete value;
                        }else{
                            if(player->pos().x() <= it.value()->pos().x()){//50

                                if(player->left_right_jump[1] == 1){
                                    player->setPos(it.value()->pos().x()-15-60, player->y());
                                }
                            }
                            //player->CollisionDoor();
                        }
                    }
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::DOORF){
                auto collding_items=this->collidingItems(it.value());

                foreach(QGraphicsItem *item, collding_items){
                    if(typeid (*item) == typeid (PlayerPinky)){
                        auto player = (qgraphicsitem_cast<PlayerPinky*>(item));
                        auto door = (qgraphicsitem_cast<DoorFinish*>(it.value()));
                            qDebug()<<"ovde";
                        if(player->num_of_stars == 4){
                            this->removeItem(it.value());
                            it.value()->setIsAlive(false);

                            Treasure *firework = new Treasure();
                            addItem(firework);
                            firework->setPos(6600, 150);
                            firework->setZValue(-1);
                            firework->setPixmap(QPixmap(":/images/other/pinky_scene/fireworks.png"));
                            firework->setScale(0.17);
                            items.insert(firework->getID(), firework);
                            firework->incID();

                            Treasure *mimi = new Treasure();
                            addItem(mimi);
                            mimi->setPos(6720, 270);
                            mimi->setZValue(-1);
                            mimi->setPixmap(QPixmap(":/images/players/mimi/mimi_1.png"));
                            mimi->setScale(0.1);
                            items.insert(mimi->getID(), mimi);
                            mimi->incID();

                            Treasure *mermaid = new Treasure();
                            addItem(mermaid);
                            mermaid->setPos(6680, 330);
                            mermaid->setZValue(1);
                            mermaid->setPixmap(QPixmap(":/images/players/mermaid/mermaid_1.png"));
                            mermaid->setScale(0.09);
                            items.insert(mermaid->getID(), mermaid);
                            mermaid->incID();

                            Treasure *koko = new Treasure();
                            addItem(koko);
                            koko->setPos(6800, 270);
                            koko->setZValue(-1);
                            koko->setPixmap(QPixmap(":/images/players/koko/koko_left.png"));
                            koko->setScale(0.1);
                            items.insert(koko->getID(), koko);
                            koko->incID();

                            Treasure *gameOver = new Treasure();
                            addItem(gameOver);
                            gameOver->setPos(6500, 50);
                            gameOver->setZValue(1);
                            gameOver->setPixmap(QPixmap(":/images/other/pinky_scene/gameOver.png"));
                            gameOver->setScale(0.5);
                            items.insert(gameOver->getID(), gameOver);
                            gameOver->incID();

                           /* Animation();
                            DeleteLevelPinky();
                            emit gameOver();
                            return;*/
                            endAnimation=true;
                        }else{
                            if(player->pos().x() <= it.value()->pos().x()){//50

                                if(player->left_right_jump[1] == 1){
                                    player->setPos(it.value()->pos().x()-15-60, player->y());
                                }
                            }
                            //player->CollisionDoor();
                        }
                    }
                }
            }
            else if(it.value()->getType() == ItemOfScene::typeOfItem::ENEMYGHOST){

                auto enemy = (qgraphicsitem_cast<EnemyFly *>(it.value()));
                if(enemy->getNearPlayer() == false){
                    enemy->findPlayer();
                }
                else if(enemy->getTimeout() == 0){
                    enemy->move(player->x(), player->y());
                    enemy->collisions();
                }
                else
                    enemy->decraseTimeout();

                enemy->image_change();

                if(enemy->getHealthChange() > 0){
                    pHealth->decrease();
                    enemy->setHealthChange(0);
                }

                if(pHealth->getHealth() <= 0){

                    DeleteLevelPinky();
                    emit lifeLost();

                    return;
                }
            /*if(enemy->getIsAlive() == false){
                    DeleteLevelPinky();
                     emit lifeLost();

                     return;
                }*/

             }
        }
    }

    //*** BRISANJE "mrtvih" elemenata ********//
    int key_check = -1;

    if(!items.empty()){
        for(auto it=items.begin(); it!=items.end(); it++){

            if(key_check != -1){

                auto values=items.take(key_check);
                //removeItem(values);
                delete values;

                key_check = -1;
            }

            if(it.value()->getIsAlive() == false){
                key_check=it.key();
            }
        }
    }

    if(key_check != -1){
        auto values=items.take(key_check);
        //removeItem(values);
        delete values;

        key_check = -1;
    }

    /*** KRETANJE IGRACA  I KOLIZIJE S PLATFORMAMA ***/

    player->move();

    player->image_change();
    player->collisions();

    if(endAnimation)
        Animation();

    emit center(player, pHealth, sScore, kScore, rules, buttonRules, musicButton);
}

void LevelPinky::onClicked()
{
    removeItem(rules);
    removeItem(buttonRules);
    pause=!pause;
    emit gamePaused();
    playSounds();
    stopEventKey=false;
    onRules=false;
}
void LevelPinky::keyPressEvent(QKeyEvent *event){

    if(event->key()==Qt::Key_P && !onRules){
       emit gamePaused();
        if(!pause ){
            stopEventKey=true;
            pauseSonuds();
        }else{
            stopEventKey=false;
            playSounds();

        }
        pause=!pause;

    }else if(event->key()==Qt::Key_M){

       setMute();
       if(mute)
           musicButton->initialize(":/images/buttons/muteWhite.png", 0.03, 5, 55);
       else
           musicButton->initialize(":/images/buttons/speakerWhite.png", 0.03, 5, 55);
       checkMute();
    }
    if(event->key()==Qt::Key_B){
        DeleteLevelPinky();
        emit gameOver();
    }
    if(event->key()==Qt::Key_1){
        DeleteLevelPinky();
        emit level(0);
        return;
    }
    if(event->key()==Qt::Key_2){
        DeleteLevelPinky();
        emit level(1);
    }
    if(event->key()==Qt::Key_3){
        DeleteLevelPinky();
        emit level(2);
    }
    if(event->key()==Qt::Key_4){
        DeleteLevelPinky();
        emit level(3);
    }

    if(event->isAutoRepeat() == false && event->key()==Qt::Key_H && !onRules){

        if(!pause){

            emit gamePaused();
            stopEventKey=true;
            pauseSonuds();
            pause=!pause;
        }
        rules->setNumberOfLevel(4);
        buttonRules->setZValue(10);
        addItem(rules);
        addItem(buttonRules);
        onRules=true;


    }
    if(stopEventKey)
        return;

    if(event->key() == Qt::Key_Left){
        player->img_side = 1;
        player->left_right_jump[0] = 1;
    }
    else if(event->key() == Qt::Key_Right){
        player->img_side = 2;
        player->left_right_jump[1] = 1;
    }
    else if(event->isAutoRepeat() == false && event->key() == Qt::Key_Up){
            if(/*player->pos().y() > 0 && player->left_right_jump[2] == 0 &&*/ player->standing){
                player->left_right_jump[2] = 1;
                player->jump_height = 16;
            }
    }

    else if(event->isAutoRepeat() == false && event->key()==Qt::Key_Space){
       if(player->img_side == 2){
        Stars *st = new Stars();
        addItem(st);
       //70
        st->setPos(player->x()+20, player->y()+10);
        items.insert(st->getID(), st);
        st->incID();
       }else if(player->img_side == 1){
        StarsL *st1 = new StarsL();
        st1->setPixmap(QPixmap(":/images/attacks/gold_stars1.png"));
        st1->setScale(0.2);
        addItem(st1);
        //90
        st1->setPos(player->x()-40, player->y()+10);
        items.insert(st1->getID(), st1);
        st1->incID();
       }

    }
}

void LevelPinky::keyReleaseEvent(QKeyEvent *event)
{
    if(event->isAutoRepeat() == false && event->key() == Qt::Key_Left){
        player->left_right_jump[0] = 0;
        //player->img_side = 1;
    }
    if(event->isAutoRepeat() == false && event->key() == Qt::Key_Right){
        player->left_right_jump[1] = 0;
        //player->img_side = 2;
    }
   if(event->isAutoRepeat() == false && event->key() == Qt::Key_Up){
        if(player->left_right_jump[2] < 10){
             player->jump_height = 10;
             //std::cout <<
        }
    }
}
void LevelPinky::addImage(QGraphicsPixmapItem *item, const QString &fileName, qreal ax, qreal ay, qreal scale)
{
    item->setPixmap(QPixmap(fileName));
    item->setScale(scale);
    item->setPos(ax, ay);
    addItem(item);
}
void LevelPinky::DeleteLevelPinky(){
    int key_value = -1;

    for(auto it = items.begin(); it != items.end(); it++){
        if(key_value != -1){
            auto value = items.take(key_value);
            removeItem(value);
            delete value;
        }
        key_value = it.key();
    }

    auto value = items.take(key_value);
    removeItem(value);
    delete value;

    for(int i = 0; i < platforme.size(); i++){
        delete platforme[i];
        platforme.pop_front();
        platforme.clear();
    }

    delete player;
    delete pHealth;
    delete sScore;
    delete kScore;
    deleteSounds();
}

void LevelPinky::Animation(){
    if(player->pos().x() <= 6630){
        player->setPos(player->x()+10,player->y());
        wait=0;
        oldWait=50;
    }
    else if(oldWait==wait){
        DeleteLevelPinky();
        emit gameOver();
        return;
    }else{
        wait++;
    }
}

void LevelPinky::onSetMute()
{
    setMute();
    if(mute==true)
        musicButton->initialize(":/images/buttons/muteWhite.png", 0.03, 5, 55);
    else
        musicButton->initialize(":/images/buttons/speakerWhite.png", 0.03, 5, 55);
    checkMute();
}
/*
void LevelPinky::addObjects(ItemOfScene::typeOfItem type, int x, int y)
{
    if(type == ItemOfScene::typeOfItem::ENEMYGHOST){
        EnemyFly* e = new EnemyFly();
        e->setPos(x*80,y*70);
        e->setType(ItemOfScene::typeOfItem::ENEMYGHOST);
        addItem(e);
        items.insert(e->getID(), e);
        e->incID();
    }
}*/
