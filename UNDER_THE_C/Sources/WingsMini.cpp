
#include <QBrush>
#include <stdlib.h>
#include "Headers/WingsMini.h"

WingsMini::WingsMini(QGraphicsItem *parent):StraightMotion(parent)
{
    setType(WINGS);
    setIsAlive(true);

    setPixmap(QPixmap(":/images/collectables/wing-clipart-5.png"));
    setScale(0.3);
    int random=rand()%700;
    setPos(random, 700);

    wings_change=false;
}

void WingsMini::collision()
{
    if(getIsAlive()==false)
        return;

    auto colliding_items = collidingItems();


     foreach (auto item, colliding_items){
         if (typeid(*item) == typeid(PlayerMimi)){
             setWingsChange(true);
             setIsAlive(false);
             return;

         }
     }
}

void WingsMini::setWingsChange(bool value)
{
    wings_change = value;
}

bool WingsMini::getWingsChange()
{
    return wings_change;
}
