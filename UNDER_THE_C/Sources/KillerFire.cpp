#include "Headers/PlayerMimi.h"

#include <Headers/KillerFire.h>


KillerFire::KillerFire(int x, int y, QGraphicsItem *parent): StraightMotion(parent)
{
    setType(KILLERFIRE);
    setIsAlive(true);

    setPixmap(QPixmap(":/images/attacks/fire-clipart-16.png"));
    setIsAlive(true);
    setPos(x,y);
    setScale(0.1);

    health_change = 0;

}


void KillerFire::collision()
{
    if(getIsAlive()==false)
        return;

    auto colliding_items = collidingItems();
    foreach (auto item, colliding_items){

        if (typeid(*item) == typeid(PlayerMimi)){
            setHealthChange(1);
            setIsAlive(false);
            return;

        }
    }
}
