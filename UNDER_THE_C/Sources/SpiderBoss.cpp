#include "Headers/SpiderBoss.h"

SpiderBoss::SpiderBoss()
{
    setPixmap(QPixmap(":/images/enemies/spider/spiderboss1"));

    setIsAlive(true);
    setType(SPIDERBOSS);

    health = 10;
    health_change = 0;

    battle = false;
    img_change = 0;
    attack1 = 0;
    attack2 = 0;
    rest = -3;
    timeout = 0;
    vulnerable = false;
    defense = 0;
    setMusicEmit(0);
}

void SpiderBoss::findPlayer()
{
    const auto zone = scene()->items(mapToScene(QRect(0,0,100,y() + 7*70)));

    for(auto item: zone){

        if(item == this)
            continue;

        if(typeid (*(item)) == typeid(PlayerPlatform)){
            setMusicEmit(1);
            battle = true;
        }
    }
}

void SpiderBoss::move(int player_x)
{
    if(attack1 == 0 && rest < 0 && player_x < 40*80){
        setPos(player_x, y());
        return;
    }
    else if (attack1 == 1){
        if(y()+20+150 < 38*70)
            setPos(x(), y() +20);
        else
            attack1 = 2;
   }
    else if(attack1 == 2){
        if(y()-20 > 25*70)
            setPos(x(), y() - 20);
        else{
            attack1 = 0;
            startDefense();
        }
    }
    else if(rest == 0){
        if(y()+20+150 < 31*70)
            setPos(21*80, y() + 20);
        else {
            rest = 2;
            vulnerable = true;
        }
    }
    else if(rest >= 2 && rest < 150){
        rest++;
    }
    else if(rest == 150){
        vulnerable = false;

        if(y()-20 > 25*70)
            setPos(x(), y()-20);
        else{
            rest = -3;
            startDefense();
        }
    }

}

void SpiderBoss::collisions()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (Sword) && vulnerable && timeout == 0){
            timeout = 10;
            health--;

            if(health == 0){
                setIsAlive(false);
                setMusicEmit(2);
            }
        }
        else if(typeid (*(colliding_items[i])) == typeid (PlayerPlatform)){
            auto player = (qgraphicsitem_cast<PlayerPlatform *>(colliding_items[i]));

            if(player->invincible == 0 && getIsAlive()){
                health_change = 1;
                player->invincible++;
            }
        }
    }

}

void SpiderBoss::image_change()
{

    if(img_change <= 4){
        setPixmap(QPixmap(":/images/enemies/spider/spiderboss1.png"));
        img_change++;
    }
    else{
        setPixmap(QPixmap(":/images/enemies/spider/spiderboss2.png"));
        img_change++;
        if(img_change >= 8)
            img_change = 0;
    }

}

bool SpiderBoss::getBattle()
{
    return battle;
}
int SpiderBoss::attack()
{
    if(battle == false)
        return 5;

    rest++;

    if(rest == 0){
        defense++;
        return 0;
    }

    int random = rand()%2 +1 ;

    switch (random) {
    case 1:
        attack1 = 1;
        break;
    case 2:
        attack2 = 1;
        break;
    default:
        break;
    }
    setMusicEmit(3);
    defense++;
    return random;
}

bool SpiderBoss::getVulnerable()
{
    return vulnerable;
}

int SpiderBoss::getTimeout()
{
    return timeout;
}

void SpiderBoss::decraseTimeout()
{
    timeout--;
}

void SpiderBoss::increaseDefense()
{
    defense++;
}

int SpiderBoss::getDefense()
{
    return defense;
}

void SpiderBoss::startDefense()
{
    defense = 0;
}
