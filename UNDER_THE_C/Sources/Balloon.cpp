#include "Headers/Balloon.h"


Balloon::Balloon(QGraphicsItem *parent):ItemOfScene(parent)
{
    setPixmap(QPixmap(":/images/collectables/ballon.png"));
    setScale(0.2);
    int random=rand()%700;
    setPos(random, 700);
    setIsAlive(true);
    setTransformOriginPoint(50,50);
    life=false;
    left=true;
    rotation=360;
    setType(BALLOON);


}

int Balloon::height() const
{
    return boundingRect().height()*0.2;
}

int Balloon::incLife()
{
    return life;
}

void Balloon::move(int speed_item)
{
    if(getIsAlive()==false)
        return;

   auto colliding_items = collidingItems();

    foreach (auto item, colliding_items){
        if (typeid(*item) == typeid(PlayerMimi)){

            life=true;
            setIsAlive(false);
            return;

        }
    }

    if(pos().x()>0 && left){
        setRotation(rotation-10);
        setPos(x()-10,y()-10-speed_item);
    }else{
        left=false;
    }

    if(pos().x()<700 && !left){
        setRotation((rotation+10)%360);
        setPos(x()+10, y()-10-speed_item);
    }else{
        left=true;
    }


    if (pos().y() + height() < 0){
        setIsAlive(false);
    }
}


