#include "Headers/EnemyDeathRay.h"

EnemyDeathRay::EnemyDeathRay(int player_x, int player_y, int enemy_x, int enemy_y)
{
   setIsAlive(true);
   setType(DEATHRAY);

   x = player_x;
   y = player_y;
   ex = enemy_x;
   ey = enemy_y;

   health_change=0;
   activated = 0;
}

QRectF EnemyDeathRay::boundingRect() const
{
    return QRect(ex-41*80*2,ey-9*70*2,80*80*2,30*70*2);
}

QPainterPath EnemyDeathRay::shape() const
{
    QPainterPath path;

    QVector<QPointF> poly = {QPointF(ex+15,ey+15), QPointF(ex+30,ey+30), QPointF(x+15,y+55), QPointF(x+30,y+70)};
    path.moveTo(0,0);
    path.addPolygon(poly);
    path.closeSubpath();

    return path;
}

void EnemyDeathRay::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(activated < 20){
        painter->setPen(QPen(Qt::white, 12, Qt::DashDotLine, Qt::RoundCap));
        painter->drawLine(x+30,y+70,ex+70,ey+60);
    }
    else{
        painter->setPen(QPen(Qt::red, 30, Qt::SolidLine, Qt::RoundCap));
        painter->drawLine(x+30,y+70,ex+70,ey+60);
    }
}

void EnemyDeathRay::collision()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (PlayerPlatform) && activated >=20){
            auto player = (qgraphicsitem_cast<PlayerPlatform *>(colliding_items[i]));

            if(player->invincible == 0 && getIsAlive()){
                health_change = 1;
                player->invincible++;
            }
        }
    }
}

void EnemyDeathRay::incActivated()
{
    activated++;
}

int EnemyDeathRay::getActivated()
{
    return activated;
}

int EnemyDeathRay::getEX()
{
    return ex;
}

int EnemyDeathRay::getEY()
{
    return ey;
}
