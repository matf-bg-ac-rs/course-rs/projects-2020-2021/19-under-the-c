#include "Headers/PlayerPlatform.h"

#include <iostream>

PlayerPlatform::PlayerPlatform(QGraphicsItem * parent):
    QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/images/players/koko/koko_right.png"));
    setScale(0.1);
    shape().toFillPolygon();

    setMusicEmit(0);

    proba = 0;
    invincible = 0;
    health_change=0;
    collected_cristals = 0;
    golden_collected = false;
    standing = false;
    music_timer = 0;
}

QPainterPath PlayerPlatform::shape() const
{
    QPainterPath path;

    path.moveTo(0,0);
    path.addRect(200,0, 400, 1400);

    return path;
}

void PlayerPlatform::image_change()
{
    //qpixmap = sve ono na kraju samo da pita da li je hit ili ne

    if(invincible == 0){
        // smena slika igraca prilikom kretanja na desnu stranu
        if(left_right_jump[1] == 1){

            if(proba <= 3)
                setPixmap(QPixmap(":/images/players/koko/koko_right_1.png"));
            else if(proba > 3 && proba <= 6)
                setPixmap(QPixmap(":/images/players/koko/koko_right_2.png"));
            else if(proba > 6){
                setPixmap(QPixmap(":/images/players/koko/koko_right_3.png"));

                if(proba == 10)
                    proba = 0;
            }

            proba++;
        }

        // smena slika igraca prilikom kretanja na levu stranu
        else if(left_right_jump[0] == 1){

            if(proba <= 3)
                setPixmap(QPixmap(":/images/players/koko/koko_left_1.png"));
            else if(proba > 3 && proba <= 6)
                setPixmap(QPixmap(":/images/players/koko/koko_left_2.png"));
            else if(proba > 6){
                setPixmap(QPixmap(":/images/players/koko/koko_left_3.png"));

                if(proba == 10)
                    proba = 1;
            }

            proba++;
        }

        // ispisivanje slike igraca dok miruje
        else /*if(left_right_jump[0] == 0 && left_right_jump[1] == 0 && proba > 0)*/{

            // promenljiva last odredjuje da li je poslednje pusten taster za levo ili za desno
            if(last == 1)
                setPixmap(QPixmap(":/images/players/koko/koko_left.png"));
            else
                setPixmap(QPixmap(":/images/players/koko/koko_right.png"));
            proba = 0;
        }
    }
    else{

        invincible++;

        if(invincible >=30)
            invincible=0;

        // smena slika igraca prilikom kretanja na desnu stranu
        if(left_right_jump[1] == 1){

            if(proba <= 3)
                setPixmap(QPixmap(":/images/players/koko/koko_right_1_hit.png"));
            else if(proba > 3 && proba <= 6)
                setPixmap(QPixmap(":/images/players/koko/koko_right_2_hit.png"));
            else if(proba > 6){
                setPixmap(QPixmap(":/images/players/koko/koko_right_3_hit.png"));

                if(proba == 10)
                    proba = 0;
            }

            proba++;
        }

        // smena slika igraca prilikom kretanja na levu stranu
        else if(left_right_jump[0] == 1){

            if(proba <= 3)
                setPixmap(QPixmap(":/images/players/koko/koko_left_1_hit.png"));
            else if(proba > 3 && proba <= 6)
                setPixmap(QPixmap(":/images/players/koko/koko_left_2_hit.png"));
            else if(proba > 6){
                setPixmap(QPixmap(":/images/players/koko/koko_left_3_hit.png"));

                if(proba == 10)
                    proba = 1;
            }

            proba++;
        }

        // ispisivanje slike igraca dok miruje
        else /*if(left_right_jump[0] == 0 && left_right_jump[1] == 0 && proba > 0)*/{

            // promenljiva last odredjuje da li je poslednje pusten taster za levo ili za desno
            if(last == 1)
                setPixmap(QPixmap(":/images/players/koko/koko_left_hit.png"));
            else
                setPixmap(QPixmap(":/images/players/koko/koko_right_hit.png"));
            proba = 0;
        }
    }
}
// Kretanje igraca ako je preitisnut neki od tastera za kretanje
void PlayerPlatform::move()
{
    // animacija pada igraca kada nije u skoku
    if(left_right_jump[2] == 0){
        setPos(x(), y() + 14);//12
    }

    // animacija skoka igraca
    if(left_right_jump[2] > 0){
        standing = false;
        setPos(x(), y() - 15);

        if(left_right_jump[2] + 1 == jump_height)
            left_right_jump[2] = 0;
        else left_right_jump[2]++;

    }

    // kretanje igraca na levu stranu
    if(left_right_jump[0] == 1 && x() > 0){
         setPos(x() - 12, y());//14
    }

    // kretanje igraca na desnu stranu
    else if(left_right_jump[1] == 1){
        setPos(x() + 12, y());
    }

    if((left_right_jump[0] == 1 || left_right_jump[1] == 1) && standing == true){
        setMusicEmit(1);
    }

}
// Obradjivanje kolizija na koje igrac treba da reaguje
void PlayerPlatform::collisions()
{
    auto colliding_items = collidingItems();
    int n = colliding_items.size();

    standing = false;

    for(int i =0; i < n; i++){

        if(typeid (*(colliding_items[i])) == typeid (Platform)){

            auto platform = (qgraphicsitem_cast<Platform *>(colliding_items[i]));
            collide_platform(platform->pos().x(), platform->pos().y(), platform->rect().width(), platform->rect().height());
        }
        else if(typeid (*(colliding_items[i])) == typeid (EnemyThorn)){
            if(x() < 50*80)
                setPos(24*80, 37*70);
            else if(y() > 25*70)
                setPos(70*80,28*70);
            else
                setPos(100*80,25*70);

            health_change = 1;
        }
        else if(typeid (*(colliding_items[i])) == typeid (Cristal)){
            auto cristal = (qgraphicsitem_cast<Cristal*>(colliding_items[i]));

            if(cristal->getType() == 'C')
                collected_cristals++;
            else if(cristal->getType() == 'K')
                golden_collected = true;
        }
        else if(typeid (*(colliding_items[i])) == typeid (Door)){
            auto door = (qgraphicsitem_cast<Door*>(colliding_items[i]));

            if(door->x() < x()){
                setPos(door->x()+door->boundingRect().width()+1, y());
            }
            else
                setPos(door->x() - boundingRect().width()*0.09, y());
        }
    }
}


int PlayerPlatform::getHealthChange()
{
    return health_change;
}

void PlayerPlatform::setHealthChange(int value)
{
    health_change = value;
}

int PlayerPlatform::getCollectedCristals()
{
    return collected_cristals;
}

bool PlayerPlatform::getGoldenCollected()
{
    return golden_collected;
}

int PlayerPlatform::getMusicTimer()
{
    return music_timer;
}

void PlayerPlatform::setMusicTimer(int value)
{
    music_timer = value;
}

void PlayerPlatform::increaseMusicTimer()
{
    music_timer++;
}

// Obrada kolizija izmedju igraca i platforme
void PlayerPlatform::collide_platform(int platform_x, int platform_y, int platform_width, int platform_height)
{
    //kolizija igraca i gornje ivice platforme
    if(pos().y() + 126 <= platform_y){
        if(left_right_jump[2] == 0)
            setPos(x(), platform_y - 140);

        standing = true;
        return;
    }

    //kolizija igraca i donje ivice platforme
    if(pos().y() + 15 >= platform_y + platform_height){//10
        if(left_right_jump[2] > 0){
            left_right_jump[2] = 0;
            //setPos(x(), y() + 15);//setPos(x(), y() - 15);
        }
    }

    // kolizija igraca i leve ivice platforme
    if(pos().x() + 48 <= platform_x){
        if(left_right_jump[1] == 1)
            setPos(platform_x-15-60, y());
    }

    //kolizija igraca i desne ivice platforme
    if(pos().x() + 32  >= platform_x + platform_width){
        if(left_right_jump[0] == 1)
            setPos(platform_x+platform_width+1,y());
    }
}

