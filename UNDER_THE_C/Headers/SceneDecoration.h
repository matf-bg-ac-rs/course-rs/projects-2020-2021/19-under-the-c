#ifndef SCENEDECORATION_H
#define SCENEDECORATION_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <Headers/StaticSceneObjects.h>

class SceneDecoration: public StaticSceneObjects, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    SceneDecoration();
    SceneDecoration(char type, int x, int y);
};

#endif // SCENEDECORATION_H
