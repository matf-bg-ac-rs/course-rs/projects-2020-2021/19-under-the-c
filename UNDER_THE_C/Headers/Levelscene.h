#ifndef LEVELSCENE_H
#define LEVELSCENE_H

#include <Headers/Music.h>
#include <QGraphicsScene>
#include <Headers/Button.h>
#include "Health.h"
#include "Score.h"
#include "Rules.h"
#include <Headers/PlayerPlatform.h>
class LevelScene: public QGraphicsScene, public Music
{
    Q_OBJECT
public:
    LevelScene(QObject *parent = nullptr);
    virtual void initialize();
    //virtual void checkMute()=0;
    int level_type;
    Button *musicButton;
    //Health* h;
public slots:
    void update()
    {}

signals:

    // aktivira se kad igrac izgubi zivot
    void lifeLost();
    // skupljen je zivot
    void lifeCollected();
    // predjen je nivo
    void levelCompleted();
    // scena prati igraca (kada svi igraci budu nasledjivali player klasu i ovo ce biti okej)
    void center(PlayerPlatform* player, Health* h, Score* s1, Score* s2, Rules* rules=nullptr,
                Button *button=nullptr, Button *buttonMute=nullptr);
    //void gamePaused();

    void gamePaused();
    void gameOver();
    void level(int brLevel);



protected:
   // Player * player;
    //static bool mute;
    Health * pHealth;
    Score * pScore;
    Score * sScore;
    Score * kScore;
    Rules *rules;

};

#endif // LEVELSCENE_H
