#ifndef ENEMY_H
#define ENEMY_H
#include <QGraphicsRectItem>
#include <QObject>
#include <QPainter>
#include<QGraphicsPixmapItem>
#include "ItemOfScene.h"
#include "Fire.h"
#include "Score.h"
#include <Headers/StraightMotion.h>
#include <Headers/EnemiesWithScore.h>
#include <QMessageBox>
class Enemy: public StraightMotion, public EnemiesWithScore{

    Q_OBJECT
public:

    Enemy(int numberOfLife=1,StraightMotion * parent=0);
    static double ubrzanje_enemy;

    //WARNING: Kad se izbaci iz Pinky nivoa izbaciti i ova dva
    int ID;
    static int brojac;

    int getNumberOfLife() const;
    void incNumberOfLife();
    void collision();
    int getDX();
    void setDX(int value);
    int getDX1();
    void setDX1(int value);
    void moveP();


private:
    QMessageBox boxMsg;
    bool direction;
    int sizeOfPath;
    int dx;
    int dx1;
};
#endif // ENEMY_H
