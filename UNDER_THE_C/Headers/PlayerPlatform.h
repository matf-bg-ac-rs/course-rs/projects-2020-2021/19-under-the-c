#ifndef PLAYERPLATFORM_H
#define PLAYERPLATFORM_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QPainterPath>
#include<QPainter>

#include <Headers/Platform.h>
#include <Headers/EnemyThorn.h>
#include <Headers/Door.h>
#include <Headers/Cristal.h>
#include <Headers/ItemWithSounds.h>

class PlayerPlatform: public QObject, public QGraphicsPixmapItem, public ItemWithSounds{
    Q_OBJECT
public:
    PlayerPlatform(QGraphicsItem *parent = nullptr);
    QPainterPath shape() const override;
    void image_change();
    void move();
    void collisions();
    int getHealthChange();
    void setHealthChange(int value);
    int getCollectedCristals();
    bool getGoldenCollected();
    int getMusicTimer();
    void setMusicTimer(int value);
    void increaseMusicTimer();

    bool standing;
    int last;
    int jump_height;
    int invincible;
    int left_right_jump[3]={0,0,0};
protected:
    void collide_platform(int platfomr_x, int platform_y, int platform_width, int platform_height);
private:
    int proba;
    int health_change;
    int collected_cristals;
    bool golden_collected;
    int music_timer;
};

#endif // PLAYERPLATFORM_H
