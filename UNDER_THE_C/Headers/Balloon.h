#ifndef BALLOON_H
#define BALLOON_H

#include "ItemOfScene.h"
#include "PlayerMimi.h"
#include <QGraphicsScene>
#include <QDebug>

class Balloon:public ItemOfScene
{
public:
    Balloon(QGraphicsItem *parent=nullptr);
    int height() const;
    int incLife();

    // ItemOfScene interface
public:
    void move(int speed_item);

private:
    bool life;
    bool left;
    int rotation;

};

#endif // BALLOON_H
