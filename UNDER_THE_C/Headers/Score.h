#ifndef SCORE_H
#define SCORE_H


#include <QGraphicsTextItem>
class Score: public QGraphicsTextItem{
public:
    Score(QGraphicsItem * parent=0);
    Score(int t);
    void move(int player_x, int player_y);
    void increase();
    void decrease();
    int decreaseByPoints(int number);
    void increaseByPoints(int number);
    int getScore();
private:
    int score;
    int type;
};


#endif // SCORE_H
