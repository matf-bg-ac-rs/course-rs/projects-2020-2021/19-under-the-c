#ifndef WINGSSCORE_H
#define WINGSSCORE_H


#include <QGraphicsTextItem>
class WingsScore: public QGraphicsTextItem{
public:
    WingsScore(QGraphicsItem * parent=0);
    bool use_wings();
    void inc_number_of_wings();
private:
    int number_of_wings;
};

#endif // WINGSSCORE_H
