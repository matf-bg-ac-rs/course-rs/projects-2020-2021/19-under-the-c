#ifndef ENEMYDEATHRAY_H
#define ENEMYDEATHRAY_H

#include <Headers/Enemies.h>
#include <Headers/ItemOfScene.h>
#include <QGraphicsScene>
#include <QPainter>
#include <Headers/PlayerPlatform.h>
#include <QVector>

class EnemyDeathRay: public ItemOfScene, public Enemies
{
public:
    EnemyDeathRay(int player_x, int player_y, int enemy_x, int enemy_y);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget=0) override;
    void collision();
    void incActivated();
    int getActivated();
    int getEX();
    int getEY();
private:
    int activated;
    int x,y;
    int ex, ey;
};

#endif // ENEMYDEATHRAY_H
