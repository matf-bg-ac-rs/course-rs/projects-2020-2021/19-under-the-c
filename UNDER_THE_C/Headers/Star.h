#ifndef STAR_H
#define STAR_H

#include <QGraphicsRectItem>
#include <QObject>
#include <Headers/StraightMotion.h>
#include <Headers/Collectable.h>

class Star:public StraightMotion, public Collectable{
    Q_OBJECT
public:
    Star(QGraphicsItem * parent=0);
    static int brojac;
    int ID;
};

#endif // STAR_H
