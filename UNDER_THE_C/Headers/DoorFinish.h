#ifndef DOORFINISH_H
#define DOORFINISH_H

#include <Headers/Platform.h>
#include <QGraphicsPixmapItem>
#include <Headers/PlayerPinky.h>
#include <Headers/StraightMotion.h>
#include <Headers/Collectable.h>

class DoorFinish:public ItemOfScene, public Collectable{
    Q_OBJECT
public:
    DoorFinish(QGraphicsItem * parent=0);
    void collision(DoorFinish* id);
    static int brojac;
    int ID;
};


#endif // DOORFINISH_H
