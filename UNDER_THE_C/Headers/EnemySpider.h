#ifndef ENEMYSPIDER_H
#define ENEMYSPIDER_H

#include <Headers/Enemies.h>
#include <Headers/ItemOfScene.h>
#include <Headers/Sword.h>
#include <Headers/PlayerPlatform.h>
#include <QGraphicsScene>
#include <Headers/Sword.h>

class EnemySpider:public ItemOfScene, public Enemies
{
public:
    EnemySpider();
    void findPlayer();
    void collision();
    void image_change();
    void decreaseTimeout();
    int getTimeout();
    bool getAttackMode();
    void setAttackMode(bool value);
private:
    int timeout;
    bool attack_mode;
    int attack_cooldown;
    int img_change;
};

#endif // ENEMYSPIDER_H
