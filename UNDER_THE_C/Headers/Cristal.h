#ifndef CRISTAL_H
#define CRISTAL_H

#include <Headers/ItemOfScene.h>
#include <Headers/Collectable.h>
#include <Headers/PlayerPlatform.h>
#include <Headers/ItemWithSounds.h>

class Cristal: public ItemOfScene, public Collectable,public ItemWithSounds
{
public:
    Cristal(char type);
    void collision();
    char getType();
    bool getGoldenCollected();

private:
    char type;
    bool golden_collected;
};

#endif // CRISTAL_H
