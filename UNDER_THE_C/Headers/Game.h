#ifndef GAME_H
#define GAME_H

#include <QObject>

#include <QTimer>
#include <QList>
#include <QGraphicsView>
#include <QApplication>
#include <Headers/Levelscene.h>
//o ovome jos razmisliti
#include <Headers/MenuScene.h>
#include <Headers/LevelMimi.h>
#include <Headers/LevelPlatform.h>
#include <Headers/LevelMermaid.h>
#include <Headers/LevelPinky.h>
#include <Headers/PlayerPlatform.h>
#include <Headers/Health.h>
#include <Headers/Button.h>
#include <Headers/Rules.h>

class Game: public QObject{
    Q_OBJECT
public:

    Game(QObject *parent = nullptr);
    ~Game();
    QGraphicsView * view;
    void addLevel(LevelScene*);
    void initialize();
    void initializeMeniItems();
    int current_level;


public slots:
    void onPlay();
    void onPlayLevel(int chosen_level);
    void onExit();
    void onLifeLost();
    void onLifeCollected();
    void onLevelCompleted();
    void onCenter(PlayerPlatform* player, Health* h, Score* s1, Score* s2, Rules* rules=nullptr, Button *button=nullptr, Button *buttonMute=nullptr);
    void onGamePaused();
    void onGameOver();
    void onLevel(int brLevel);

private:
    void disconnectAll(const QObject* sender);
    void connectAll(const QObject* sender);
    QList<LevelScene*> levels;

    QTimer* timer;
    int numberOfLifes;
    MenuScene *menu_scene;
};

#endif // GAME_H
