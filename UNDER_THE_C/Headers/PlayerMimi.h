#ifndef PLAYERMIMI_H
#define PLAYERMIMI_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>

class PlayerMimi:public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    PlayerMimi(QGraphicsItem * parent=0);
    //void keyPressEvent(QKeyEvent * event);
    QGraphicsPixmapItem *fire;
    int get_number_of_fires();
    void in_number_of_fires();
    bool getIsAlive();
    void setIsAlive(bool alive);
    void moveLeft();
    void moveRight();
    void image_change();


private:
    int number_of_fires;
    bool isAlive;

    int img_change;


};

#endif // PLAYERMIMI_H
