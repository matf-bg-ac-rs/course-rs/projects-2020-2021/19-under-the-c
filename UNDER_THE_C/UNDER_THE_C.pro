QT       += core gui \
        multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
CONFIG += resources_big

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Sources/Balloon.cpp \
    Sources/Button.cpp \
    Sources/Collectable.cpp \
    Sources/Cristal.cpp \
    Sources/Door.cpp \
    Sources/DoorFinish.cpp \
    Sources/DoorPinky.cpp \
    Sources/Enemies.cpp \
    Sources/EnemiesWithScore.cpp \
    Sources/EnemyDeathRay.cpp \
    Sources/EnemySpider.cpp \
    Sources/EnemyOctopus.cpp \
    Sources/EnemyL.cpp \
    Sources/EnemyThorn.cpp \
    Sources/Fire.cpp \
    Sources/HealthBar.cpp \
    Sources/Bubble.cpp \
    Sources/Cloud.cpp \
    Sources/Enemy.cpp \
    Sources/EnemyFly.cpp \
    Sources/EnemyofMermaid.cpp \
    Sources/Game.cpp \
    Sources/Health.cpp \
    Sources/Ink.cpp \
    Sources/ItemOfScene.cpp \
    Sources/ItemWithSounds.cpp \
    Sources/Key.cpp \
    Sources/KeyPinky.cpp \
    Sources/KillerEnemy.cpp \
    Sources/KillerFire.cpp \
    Sources/LevelMermaid.cpp \
    Sources/LevelMimi.cpp \
    Sources/LevelPinky.cpp \
    Sources/LevelPlatform.cpp \
    Sources/Levelscene.cpp \
    Sources/MenuScene.cpp \
    Sources/Music.cpp \
    Sources/Platform.cpp \
    Sources/PlayerMermaid.cpp \
    Sources/PlayerMimi.cpp \
    Sources/PlayerPinky.cpp \
    Sources/PlayerPlatform.cpp \
    Sources/Rules.cpp \
    Sources/SceneDecoration.cpp \
    Sources/Score.cpp \
    Sources/Shell.cpp \
    Sources/SpiderBoss.cpp \
    Sources/StaticSceneObjects.cpp \
    Sources/Stars.cpp \
    Sources/StarsL.cpp \
    Sources/StraightMotion.cpp \
    Sources/Sword.cpp \
    Sources/TextOfNotice.cpp \
    Sources/Timer.cpp \
    Sources/Star.cpp \
    Sources/Treasure.cpp \
    Sources/WingsMini.cpp \
    Sources/WingsScore.cpp \
    Sources/main.cpp \
    Sources/mainwindow.cpp

HEADERS += \
    Headers/Bubble.h \
    Headers/Button.h \
    Headers/Cloud.h \
    Headers/Collectable.h \
    Headers/Cristal.h \
    Headers/Door.h \
    Headers/DoorFinish.h \
    Headers/DoorPinky.h \
    Headers/Enemies.h \
    Headers/EnemiesWithScore.h \
    Headers/Enemy.h \
    Headers/EnemyDeathRay.h \
    Headers/EnemySpider.h \
    Headers/EnemyOctopus.h \
    Headers/EnemyL.h \
    Headers/EnemyThorn.h \
    Headers/Fire.h \
    Headers/EnemyofMermaid.h \
    Headers/Balloon.h \
    Headers/Cloud.h \
    Headers/EnemyFly.h \
    Headers/Game.h \
    Headers/Health.h \
    Headers/HealthBar.h \
    Headers/Ink.h \
    Headers/ItemOfScene.h \
    Headers/ItemWithSounds.h \
    Headers/Key.h \
    Headers/KeyPinky.h \
    Headers/KillerEnemy.h \
    Headers/KillerFire.h \
    Headers/LevelMermaid.h \
    Headers/LevelMimi.h \
    Headers/LevelPinky.h \
    Headers/LevelPlatform.h \
    Headers/Levelscene.h \
    Headers/MenuScene.h \
    Headers/Music.h \
    Headers/Platform.h \
    Headers/PlayerMermaid.h \
    Headers/PlayerMimi.h \
    Headers/PlayerPinky.h \
    Headers/PlayerPlatform.h \
    Headers/Rules.h \
    Headers/SceneDecoration.h \
    Headers/Score.h \
    Headers/Shell.h \
    Headers/SpiderBoss.h \
    Headers/StaticSceneObjects.h \
    Headers/Stars.h \
    Headers/StarsL.h \
    Headers/StraightMotion.h \
    Headers/Sword.h \
    Headers/TextOfNotice.h \
    Headers/Timer.h \
    Headers/Star.h \
    Headers/Treasure.h \
    Headers/WingsMini.h \
    Headers/WingsScore.h \
    Headers/mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resurs.qrc

    st.qrc

DISTFILES += \
    ../../project/19-under-the-c/UNDER_THE_C/backgrounds/under the c-pocetna.jpg \
    ../../project/19-under-the-c/UNDER_THE_C/players_images/milana.png

